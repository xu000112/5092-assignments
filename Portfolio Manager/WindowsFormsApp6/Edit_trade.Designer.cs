﻿namespace WindowsFormsApp6
{
    partial class Edit_trade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isBuyDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.quantityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timeStampDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.marketPriceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pLDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deltaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gammaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.thetaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rhoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vegaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tickerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.instrumentSetIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tradesSetSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._5092DataBaseDataSet = new WindowsFormsApp6._5092DataBaseDataSet();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tradesSetSetTableAdapter = new WindowsFormsApp6._5092DataBaseDataSetTableAdapters.TradesSetSetTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tradesSetSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._5092DataBaseDataSet)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.HighlightText;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.isBuyDataGridViewCheckBoxColumn,
            this.quantityDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn,
            this.timeStampDataGridViewTextBoxColumn,
            this.marketPriceDataGridViewTextBoxColumn,
            this.pLDataGridViewTextBoxColumn,
            this.deltaDataGridViewTextBoxColumn,
            this.gammaDataGridViewTextBoxColumn,
            this.thetaDataGridViewTextBoxColumn,
            this.rhoDataGridViewTextBoxColumn,
            this.vegaDataGridViewTextBoxColumn,
            this.tickerDataGridViewTextBoxColumn,
            this.instrumentSetIdDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.tradesSetSetBindingSource;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView1.Location = new System.Drawing.Point(12, 27);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(737, 411);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // isBuyDataGridViewCheckBoxColumn
            // 
            this.isBuyDataGridViewCheckBoxColumn.DataPropertyName = "IsBuy";
            this.isBuyDataGridViewCheckBoxColumn.HeaderText = "IsBuy";
            this.isBuyDataGridViewCheckBoxColumn.Name = "isBuyDataGridViewCheckBoxColumn";
            // 
            // quantityDataGridViewTextBoxColumn
            // 
            this.quantityDataGridViewTextBoxColumn.DataPropertyName = "Quantity";
            this.quantityDataGridViewTextBoxColumn.HeaderText = "Quantity";
            this.quantityDataGridViewTextBoxColumn.Name = "quantityDataGridViewTextBoxColumn";
            // 
            // priceDataGridViewTextBoxColumn
            // 
            this.priceDataGridViewTextBoxColumn.DataPropertyName = "Price";
            this.priceDataGridViewTextBoxColumn.HeaderText = "Price";
            this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
            // 
            // timeStampDataGridViewTextBoxColumn
            // 
            this.timeStampDataGridViewTextBoxColumn.DataPropertyName = "TimeStamp";
            this.timeStampDataGridViewTextBoxColumn.HeaderText = "TimeStamp";
            this.timeStampDataGridViewTextBoxColumn.Name = "timeStampDataGridViewTextBoxColumn";
            // 
            // marketPriceDataGridViewTextBoxColumn
            // 
            this.marketPriceDataGridViewTextBoxColumn.DataPropertyName = "MarketPrice";
            this.marketPriceDataGridViewTextBoxColumn.HeaderText = "MarketPrice";
            this.marketPriceDataGridViewTextBoxColumn.Name = "marketPriceDataGridViewTextBoxColumn";
            // 
            // pLDataGridViewTextBoxColumn
            // 
            this.pLDataGridViewTextBoxColumn.DataPropertyName = "PL";
            this.pLDataGridViewTextBoxColumn.HeaderText = "PL";
            this.pLDataGridViewTextBoxColumn.Name = "pLDataGridViewTextBoxColumn";
            // 
            // deltaDataGridViewTextBoxColumn
            // 
            this.deltaDataGridViewTextBoxColumn.DataPropertyName = "Delta";
            this.deltaDataGridViewTextBoxColumn.HeaderText = "Delta";
            this.deltaDataGridViewTextBoxColumn.Name = "deltaDataGridViewTextBoxColumn";
            // 
            // gammaDataGridViewTextBoxColumn
            // 
            this.gammaDataGridViewTextBoxColumn.DataPropertyName = "Gamma";
            this.gammaDataGridViewTextBoxColumn.HeaderText = "Gamma";
            this.gammaDataGridViewTextBoxColumn.Name = "gammaDataGridViewTextBoxColumn";
            // 
            // thetaDataGridViewTextBoxColumn
            // 
            this.thetaDataGridViewTextBoxColumn.DataPropertyName = "Theta";
            this.thetaDataGridViewTextBoxColumn.HeaderText = "Theta";
            this.thetaDataGridViewTextBoxColumn.Name = "thetaDataGridViewTextBoxColumn";
            // 
            // rhoDataGridViewTextBoxColumn
            // 
            this.rhoDataGridViewTextBoxColumn.DataPropertyName = "Rho";
            this.rhoDataGridViewTextBoxColumn.HeaderText = "Rho";
            this.rhoDataGridViewTextBoxColumn.Name = "rhoDataGridViewTextBoxColumn";
            // 
            // vegaDataGridViewTextBoxColumn
            // 
            this.vegaDataGridViewTextBoxColumn.DataPropertyName = "Vega";
            this.vegaDataGridViewTextBoxColumn.HeaderText = "Vega";
            this.vegaDataGridViewTextBoxColumn.Name = "vegaDataGridViewTextBoxColumn";
            // 
            // tickerDataGridViewTextBoxColumn
            // 
            this.tickerDataGridViewTextBoxColumn.DataPropertyName = "Ticker";
            this.tickerDataGridViewTextBoxColumn.HeaderText = "Ticker";
            this.tickerDataGridViewTextBoxColumn.Name = "tickerDataGridViewTextBoxColumn";
            // 
            // instrumentSetIdDataGridViewTextBoxColumn
            // 
            this.instrumentSetIdDataGridViewTextBoxColumn.DataPropertyName = "InstrumentSetId";
            this.instrumentSetIdDataGridViewTextBoxColumn.HeaderText = "InstrumentSetId";
            this.instrumentSetIdDataGridViewTextBoxColumn.Name = "instrumentSetIdDataGridViewTextBoxColumn";
            // 
            // tradesSetSetBindingSource
            // 
            this.tradesSetSetBindingSource.DataMember = "TradesSetSet";
            this.tradesSetSetBindingSource.DataSource = this._5092DataBaseDataSet;
            // 
            // _5092DataBaseDataSet
            // 
            this._5092DataBaseDataSet.DataSetName = "_5092DataBaseDataSet";
            this._5092DataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(761, 25);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(47, 21);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(40, 21);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // tradesSetSetTableAdapter
            // 
            this.tradesSetSetTableAdapter.ClearBeforeFill = true;
            // 
            // Edit_trade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HighlightText;
            this.ClientSize = new System.Drawing.Size(761, 450);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Edit_trade";
            this.Text = "Edit_trade";
            this.Load += new System.EventHandler(this.Edit_trade_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tradesSetSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._5092DataBaseDataSet)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private _5092DataBaseDataSet _5092DataBaseDataSet;
        private System.Windows.Forms.BindingSource tradesSetSetBindingSource;
        private _5092DataBaseDataSetTableAdapters.TradesSetSetTableAdapter tradesSetSetTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isBuyDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn timeStampDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn marketPriceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pLDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deltaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gammaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn thetaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rhoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vegaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tickerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn instrumentSetIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    }
}