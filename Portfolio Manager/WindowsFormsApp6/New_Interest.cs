﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp6
{
    public partial class New_Interest : Form
    {
        public New_Interest()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ActiveForm.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Model1Container data = new Model1Container();
            data.InterestRateSetSet.Add(new InterestRateSet
            {
                Tenor = Convert.ToInt32(interest_tenor.Text),
                Rate = Convert.ToDouble(interest_rate.Text),
            });
            data.SaveChanges();
            interest_rate.Text = "";
            interest_tenor.Text = "";
        }
    }
}
