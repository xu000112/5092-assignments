﻿namespace WindowsFormsApp6
{
    partial class New_Instrument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(New_Instrument));
            this.label1 = new System.Windows.Forms.Label();
            this.company = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ticker = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.exchange = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.strike = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tenor = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.iscall = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.barrier = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.boundary = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.rebate = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.type = new System.Windows.Forms.ComboBox();
            this.underlying = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(-1, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "Company Name (string):";
            // 
            // company
            // 
            this.company.Location = new System.Drawing.Point(157, 35);
            this.company.Name = "company";
            this.company.Size = new System.Drawing.Size(129, 21);
            this.company.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(54, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ticker (string):";
            // 
            // ticker
            // 
            this.ticker.Location = new System.Drawing.Point(157, 62);
            this.ticker.Name = "ticker";
            this.ticker.Size = new System.Drawing.Size(129, 21);
            this.ticker.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(33, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "Exchange (string):";
            // 
            // exchange
            // 
            this.exchange.Location = new System.Drawing.Point(157, 89);
            this.exchange.Name = "exchange";
            this.exchange.Size = new System.Drawing.Size(129, 21);
            this.exchange.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(27, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "Underlying (string):";
            // 
            // strike
            // 
            this.strike.Location = new System.Drawing.Point(157, 142);
            this.strike.Name = "strike";
            this.strike.Size = new System.Drawing.Size(129, 21);
            this.strike.TabIndex = 8;
            this.strike.TextChanged += new System.EventHandler(this.strike_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(40, 145);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "Strike Price (int):";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(51, 172);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "Tenor (double):";
            // 
            // tenor
            // 
            this.tenor.Location = new System.Drawing.Point(157, 169);
            this.tenor.Name = "tenor";
            this.tenor.Size = new System.Drawing.Size(129, 21);
            this.tenor.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(76, 199);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 12);
            this.label7.TabIndex = 12;
            this.label7.Text = "Call or Put:";
            // 
            // iscall
            // 
            this.iscall.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iscall.FormattingEnabled = true;
            this.iscall.Items.AddRange(new object[] {
            "Call",
            "Put"});
            this.iscall.Location = new System.Drawing.Point(157, 196);
            this.iscall.Name = "iscall";
            this.iscall.Size = new System.Drawing.Size(129, 22);
            this.iscall.TabIndex = 13;
            this.iscall.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Verdana", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(64, 225);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 12);
            this.label8.TabIndex = 14;
            this.label8.Text = "Barrier Type:";
            // 
            // barrier
            // 
            this.barrier.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barrier.FormattingEnabled = true;
            this.barrier.Items.AddRange(new object[] {
            "Down In",
            "Down Out",
            "Up In",
            "Up Out",
            "None"});
            this.barrier.Location = new System.Drawing.Point(157, 222);
            this.barrier.Name = "barrier";
            this.barrier.Size = new System.Drawing.Size(129, 22);
            this.barrier.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Verdana", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(51, 251);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 12);
            this.label9.TabIndex = 16;
            this.label9.Text = "Boundary (int):";
            // 
            // boundary
            // 
            this.boundary.Location = new System.Drawing.Point(157, 248);
            this.boundary.Name = "boundary";
            this.boundary.Size = new System.Drawing.Size(129, 21);
            this.boundary.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Verdana", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(65, 278);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 12);
            this.label10.TabIndex = 18;
            this.label10.Text = "Rebate (int):";
            // 
            // rebate
            // 
            this.rebate.Location = new System.Drawing.Point(157, 275);
            this.rebate.Name = "rebate";
            this.rebate.Size = new System.Drawing.Size(129, 21);
            this.rebate.TabIndex = 19;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(62, 290);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(0, 12);
            this.label11.TabIndex = 20;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Verdana", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(66, 12);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 12);
            this.label12.TabIndex = 21;
            this.label12.Text = "Option Type:";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.HighlightText;
            this.button1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(31, 314);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(101, 37);
            this.button1.TabIndex = 23;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.HighlightText;
            this.button2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(185, 314);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(101, 37);
            this.button2.TabIndex = 24;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // type
            // 
            this.type.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.type.FormattingEnabled = true;
            this.type.Location = new System.Drawing.Point(157, 9);
            this.type.Name = "type";
            this.type.Size = new System.Drawing.Size(129, 22);
            this.type.TabIndex = 26;
            // 
            // underlying
            // 
            this.underlying.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.underlying.FormattingEnabled = true;
            this.underlying.Location = new System.Drawing.Point(157, 116);
            this.underlying.Name = "underlying";
            this.underlying.Size = new System.Drawing.Size(129, 22);
            this.underlying.TabIndex = 27;
            this.underlying.SelectedIndexChanged += new System.EventHandler(this.underlying_SelectedIndexChanged);
            // 
            // New_Instrument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HighlightText;
            this.ClientSize = new System.Drawing.Size(300, 362);
            this.Controls.Add(this.underlying);
            this.Controls.Add(this.type);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.rebate);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.boundary);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.barrier);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.iscall);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tenor);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.strike);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.exchange);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ticker);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.company);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "New_Instrument";
            this.Text = "New Option";
            this.Load += new System.EventHandler(this.New_Instrument_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox company;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ticker;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox exchange;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox strike;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tenor;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox iscall;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox barrier;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox boundary;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox rebate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox type;
        private System.Windows.Forms.ComboBox underlying;
    }
}