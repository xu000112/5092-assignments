﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp6
{
    public partial class Edit_interest : Form
    {
        public Edit_interest()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ActiveForm.Close();
        }

        private void Edit_interest_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“_5092DataBaseDataSet1.InterestRateSetSet”中。您可以根据需要移动或删除它。
            this.interestRateSetSetTableAdapter.Fill(this._5092DataBaseDataSet1.InterestRateSetSet);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.interestRateSetSetTableAdapter.Update(this._5092DataBaseDataSet1.InterestRateSetSet);
        }
    }
}
