﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp6
{
    public partial class New_Histprice : Form
    {
        public New_Histprice()
        {
            InitializeComponent();
        }

        private void New_Histprice_Load(object sender, EventArgs e)
        { 
            Model1Container data = new Model1Container();
            var example = from a in data.InstrumentSetSet
                          join b in data.InstTypeSetSet on a.InstTypeSetId equals b.Id
                          select new { a.Id, a.CompanyName, b.TypeName};
            foreach (var i in example)
            {
                string[] str= i.TypeName.Split(' ');
                instrument.Items.Add(i.Id + " " + str[0] + " " +i.CompanyName);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Model1Container data = new Model1Container();
            string[] id = instrument.Text.Split(' ');
            int idd = Convert.ToInt32(id[0]);
            data.PriceSetSet.Add(new PriceSet
            {
                InstrumentSetId = idd,
                Date = Convert.ToDateTime(date.Text),
                ClosingPrice = Convert.ToDouble(closing_price.Text)
            });
            data.SaveChanges();
            instrument.Text = "";
            date.Text = "";
            closing_price.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ActiveForm.Close();
        }

        private void instrument_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void date_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
