﻿namespace WindowsFormsApp6
{
    partial class Edit_instrument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.companyNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tickerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.exchangeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.underlyingDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.strikeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tenorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isCallDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.barrierTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.barriersDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rebateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.instTypeSetIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.instrumentSetSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._5092DataBaseDataSet3 = new WindowsFormsApp6._5092DataBaseDataSet3();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.instrumentSetSetTableAdapter = new WindowsFormsApp6._5092DataBaseDataSet3TableAdapters.InstrumentSetSetTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instrumentSetSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._5092DataBaseDataSet3)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.HighlightText;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.companyNameDataGridViewTextBoxColumn,
            this.tickerDataGridViewTextBoxColumn,
            this.exchangeDataGridViewTextBoxColumn,
            this.underlyingDataGridViewTextBoxColumn,
            this.strikeDataGridViewTextBoxColumn,
            this.tenorDataGridViewTextBoxColumn,
            this.isCallDataGridViewTextBoxColumn,
            this.barrierTypeDataGridViewTextBoxColumn,
            this.barriersDataGridViewTextBoxColumn,
            this.rebateDataGridViewTextBoxColumn,
            this.instTypeSetIdDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.instrumentSetSetBindingSource;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.HighlightText;
            this.dataGridView1.Location = new System.Drawing.Point(12, 28);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(568, 340);
            this.dataGridView1.TabIndex = 0;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // companyNameDataGridViewTextBoxColumn
            // 
            this.companyNameDataGridViewTextBoxColumn.DataPropertyName = "CompanyName";
            this.companyNameDataGridViewTextBoxColumn.HeaderText = "CompanyName";
            this.companyNameDataGridViewTextBoxColumn.Name = "companyNameDataGridViewTextBoxColumn";
            // 
            // tickerDataGridViewTextBoxColumn
            // 
            this.tickerDataGridViewTextBoxColumn.DataPropertyName = "Ticker";
            this.tickerDataGridViewTextBoxColumn.HeaderText = "Ticker";
            this.tickerDataGridViewTextBoxColumn.Name = "tickerDataGridViewTextBoxColumn";
            // 
            // exchangeDataGridViewTextBoxColumn
            // 
            this.exchangeDataGridViewTextBoxColumn.DataPropertyName = "Exchange";
            this.exchangeDataGridViewTextBoxColumn.HeaderText = "Exchange";
            this.exchangeDataGridViewTextBoxColumn.Name = "exchangeDataGridViewTextBoxColumn";
            // 
            // underlyingDataGridViewTextBoxColumn
            // 
            this.underlyingDataGridViewTextBoxColumn.DataPropertyName = "Underlying";
            this.underlyingDataGridViewTextBoxColumn.HeaderText = "Underlying";
            this.underlyingDataGridViewTextBoxColumn.Name = "underlyingDataGridViewTextBoxColumn";
            // 
            // strikeDataGridViewTextBoxColumn
            // 
            this.strikeDataGridViewTextBoxColumn.DataPropertyName = "Strike";
            this.strikeDataGridViewTextBoxColumn.HeaderText = "Strike";
            this.strikeDataGridViewTextBoxColumn.Name = "strikeDataGridViewTextBoxColumn";
            // 
            // tenorDataGridViewTextBoxColumn
            // 
            this.tenorDataGridViewTextBoxColumn.DataPropertyName = "Tenor";
            this.tenorDataGridViewTextBoxColumn.HeaderText = "Tenor";
            this.tenorDataGridViewTextBoxColumn.Name = "tenorDataGridViewTextBoxColumn";
            // 
            // isCallDataGridViewTextBoxColumn
            // 
            this.isCallDataGridViewTextBoxColumn.DataPropertyName = "IsCall";
            this.isCallDataGridViewTextBoxColumn.HeaderText = "IsCall";
            this.isCallDataGridViewTextBoxColumn.Name = "isCallDataGridViewTextBoxColumn";
            // 
            // barrierTypeDataGridViewTextBoxColumn
            // 
            this.barrierTypeDataGridViewTextBoxColumn.DataPropertyName = "BarrierType";
            this.barrierTypeDataGridViewTextBoxColumn.HeaderText = "BarrierType";
            this.barrierTypeDataGridViewTextBoxColumn.Name = "barrierTypeDataGridViewTextBoxColumn";
            // 
            // barriersDataGridViewTextBoxColumn
            // 
            this.barriersDataGridViewTextBoxColumn.DataPropertyName = "Barriers";
            this.barriersDataGridViewTextBoxColumn.HeaderText = "Barriers";
            this.barriersDataGridViewTextBoxColumn.Name = "barriersDataGridViewTextBoxColumn";
            // 
            // rebateDataGridViewTextBoxColumn
            // 
            this.rebateDataGridViewTextBoxColumn.DataPropertyName = "Rebate";
            this.rebateDataGridViewTextBoxColumn.HeaderText = "Rebate";
            this.rebateDataGridViewTextBoxColumn.Name = "rebateDataGridViewTextBoxColumn";
            // 
            // instTypeSetIdDataGridViewTextBoxColumn
            // 
            this.instTypeSetIdDataGridViewTextBoxColumn.DataPropertyName = "InstTypeSetId";
            this.instTypeSetIdDataGridViewTextBoxColumn.HeaderText = "InstTypeSetId";
            this.instTypeSetIdDataGridViewTextBoxColumn.Name = "instTypeSetIdDataGridViewTextBoxColumn";
            // 
            // instrumentSetSetBindingSource
            // 
            this.instrumentSetSetBindingSource.DataMember = "InstrumentSetSet";
            this.instrumentSetSetBindingSource.DataSource = this._5092DataBaseDataSet3;
            // 
            // _5092DataBaseDataSet3
            // 
            this._5092DataBaseDataSet3.DataSetName = "_5092DataBaseDataSet3";
            this._5092DataBaseDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.editToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(592, 25);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(47, 21);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(42, 21);
            this.editToolStripMenuItem.Text = "Edit";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // instrumentSetSetTableAdapter
            // 
            this.instrumentSetSetTableAdapter.ClearBeforeFill = true;
            // 
            // Edit_instrument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 380);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Edit_instrument";
            this.Text = "Edit_instrument";
            this.Load += new System.EventHandler(this.Edit_instrument_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instrumentSetSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._5092DataBaseDataSet3)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private _5092DataBaseDataSet3 _5092DataBaseDataSet3;
        private System.Windows.Forms.BindingSource instrumentSetSetBindingSource;
        private _5092DataBaseDataSet3TableAdapters.InstrumentSetSetTableAdapter instrumentSetSetTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn companyNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tickerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn exchangeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn underlyingDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn strikeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tenorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn isCallDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn barrierTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn barriersDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rebateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn instTypeSetIdDataGridViewTextBoxColumn;
    }
}