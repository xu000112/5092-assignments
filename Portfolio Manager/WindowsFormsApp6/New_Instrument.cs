﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp6
{
    public partial class New_Instrument : Form
    {
        public New_Instrument()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ActiveForm.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool instruct = true;
            if (type.Text == "Barrier Option")
            {
                if (barrier.Text == "Down In" || barrier.Text == "Down Out")
                {
                    if (Convert.ToDouble(strike.Text) < Convert.ToDouble(boundary.Text))
                    {
                        instruct = false;
                        boundary.BackColor = System.Drawing.Color.Red;
                    }
                }
                else if (barrier.Text == "Up In" || barrier.Text == "Up Out")
                {
                    if (Convert.ToDouble(boundary.Text) < Convert.ToDouble(strike.Text))
                    {
                        instruct = false;
                        boundary.BackColor = System.Drawing.Color.Red;
                    }
                }
            }
            if (instruct == true)
            {
                Model1Container data = new Model1Container();
                var id = from a in data.InstTypeSetSet
                         where a.TypeName == "Stock"
                         select new { stock = a.Id };
                var example = from a in data.InstrumentSetSet
                              where a.InstTypeSetId == id.FirstOrDefault().stock
                              select a.InstTypeSetId;
                if (example.Count() != 0)
                {
                    var insttype = from a in data.InstTypeSetSet
                                   where a.TypeName == type.Text
                                   select new { stock = a.Id };
                    data.InstrumentSetSet.Add(new InstrumentSet
                    {
                        CompanyName = company.Text,
                        Ticker = ticker.Text,
                        Exchange = exchange.Text,
                        Underlying = underlying.Text,
                        Strike = Convert.ToInt32(strike.Text),
                        Tenor = Convert.ToDouble(tenor.Text),
                        IsCall = iscall.Text,
                        BarrierType = barrier.Text,
                        Barriers = Convert.ToInt32(boundary.Text),
                        Rebate = Convert.ToInt32(rebate.Text),
                        InstTypeSetId = insttype.First().stock
                    });
                    data.SaveChanges();
                    company.Text = "";
                    ticker.Text = "";
                    exchange.Text = "";
                    underlying.Text = "";
                    strike.Text = "";
                    tenor.Text = "";
                    iscall.Text = "";
                    barrier.Text = "";
                    boundary.Text = "";
                    rebate.Text = "";
                    type.Text = "";
                }
                else if (example.Count() == 0)
                {
                    var insttype = from a in data.InstTypeSetSet
                                   where a.TypeName == type.Text
                                   select new { stock = a.Id };
                    data.InstrumentSetSet.Add(new InstrumentSet
                    {
                        CompanyName = company.Text,
                        Ticker = ticker.Text,
                        Exchange = exchange.Text,
                        Underlying = underlying.Text,
                        Strike = Convert.ToInt32(strike.Text),
                        Tenor = Convert.ToDouble(tenor.Text),
                        IsCall = iscall.Text,
                        BarrierType = barrier.Text,
                        Barriers = Convert.ToInt32(boundary.Text),
                        Rebate = Convert.ToInt32(rebate.Text),
                        InstTypeSetId = insttype.First().stock
                    });
                    data.SaveChanges();
                    company.Text = "";
                    ticker.Text = "";
                    exchange.Text = "";
                    underlying.Text = "";
                    strike.Text = "";
                    tenor.Text = "";
                    iscall.Text = "";
                    barrier.Text = "";
                    boundary.Text = "";
                    rebate.Text = "";
                    type.Text = "";
                }
            }
        }

        private void strike_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void underlying_TextChanged(object sender, EventArgs e)
        {

        }

        private void underlying_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void New_Instrument_Load(object sender, EventArgs e)
        {
            Model1Container data = new Model1Container();
            var lastitem = from a in data.InstrumentSetSet
                           where a.InstTypeSetId == 3
                           orderby a.Id descending
                           select a;
            foreach (var i in lastitem)
            {
                if (underlying.Items.Contains(i.Id) == false)
                {
                    underlying.Items.Add(Convert.ToString(i.Id + " " + i.CompanyName));
                }
            }
            var instrumengtype = from a in data.InstTypeSetSet
                                 where a.TypeName != "Stock"
                                 select a;
            foreach (var i in instrumengtype)
            {
                type.Items.Add(i.TypeName);
            }
        }
    }
}