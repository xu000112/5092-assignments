﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp6
{
    public partial class New_Trade : Form
    {
        public New_Trade()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ActiveForm.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double marketprice = 0, pl = 0, delta = 0, gamma = 0, theta = 0, rho = 0, vega = 0, r = 0;
            Model1Container data = new Model1Container();
            // Get Instrument
            string[] idd = tradeid.Text.Split(' ');
            int insid = Convert.ToInt32(idd[2]);
            var example = from a in data.InstrumentSetSet
                          where a.Id == insid
                          select a;
            int typeid = Convert.ToInt32(idd[0]);
            var example2 = from a in data.InstTypeSetSet
                           where a.Id == typeid
                           select a;
            var model = from a in data.InstrumentSetSet
                        join b in data.InstTypeSetSet on a.InstTypeSetId equals b.Id
                        select new { a, b};
            var interest = from a in data.InterestRateSetSet
                           select a;
            foreach (var i in model)
            {
                bool instruct = false;
                double ceil_tenor = 0;
                double floor_tenor = 0;
                double ceil_rate = 0;
                double floor_rate = 0;
                foreach (var j in interest.Distinct())
                {
                    if (j.Tenor - i.a.Tenor == 0)
                    {
                        r = j.Rate;
                        instruct = true;
                        break;
                    }
                    else
                    {
                        if (j.Tenor - i.a.Tenor < 0)
                        {
                            if (Math.Abs(j.Tenor - i.a.Tenor) < Math.Abs(ceil_tenor - i.a.Tenor))
                            {
                                ceil_tenor = j.Tenor;
                                ceil_rate = j.Rate;
                            }
                        }
                        if (j.Tenor - i.a.Tenor > 0)
                        {
                            if (Math.Abs(j.Tenor - i.a.Tenor) < Math.Abs(floor_tenor - i.a.Tenor))
                            {
                                floor_tenor = j.Tenor;
                                floor_rate = j.Rate;
                            }
                        }
                    }
                }
                if (instruct == false)
                {
                    r = (i.a.Tenor - ceil_tenor) / (floor_tenor - ceil_tenor) * (floor_rate - ceil_rate) + ceil_rate;
                }
            }
            Montcalo.Main instrument = new Montcalo.Main();
            double v = Form1.volotility;
            double k = example.First().Strike;
            double t = example.First().Tenor;
            double[] result = new double[12];
            // stock
            if (example2.FirstOrDefault().TypeName == "Stock")
            {
                var ss = from a in data.InstrumentSetSet
                         where a.Id == insid
                         select a.Strike;
                result = instrument.Stock(ss.First(), r, v, t);
            }
            // European Option
            else if (example2.FirstOrDefault().TypeName == "European Option")
            {
                var ss = from a in data.InstrumentSetSet
                         where a.Id == insid
                         select a;
                string[] iDD = ss.First().Underlying.Split(' ');
                int realid = Convert.ToInt32(iDD[0]);
                var sss = from a in data.InstrumentSetSet
                          where a.Id == realid
                          select a.Strike;
                double s = sss.First();
                result = instrument.European(s, k, v, t, r);
            }
            // Asian option
            else if (example2.FirstOrDefault().TypeName == "Asian Option")
            {
                var ss = from a in data.InstrumentSetSet
                         where a.Id == insid
                         select a;
                string[] iDD = ss.First().Underlying.Split(' ');
                int realid = Convert.ToInt32(iDD[0]);
                var sss = from a in data.InstrumentSetSet
                          where a.Id == realid
                          select a.Strike;
                result = instrument.Asia(sss.First(), k, v, t, r);
            }
            // Barrier option !!!
            else if (example2.FirstOrDefault().TypeName == "Barrier Option")
            {
                var ss = from a in data.InstrumentSetSet
                         where a.Id == insid
                         select a;
                string[] iDD = ss.First().Underlying.Split(' ');
                int realid = Convert.ToInt32(iDD[0]);
                var sss = from a in data.InstrumentSetSet
                          where a.Id == realid
                          select a.Strike;
                double boundary = example.First().Barriers;
                bool upin = false, upout = false, downin = false, downout = false;
                if (example.First().BarrierType == "Down In")
                { downin = true; }
                else if (example.First().BarrierType == "Down Out")
                { downout = true; }
                else if (example.First().BarrierType == "Up In")
                { upin = true; }
                else if (example.First().BarrierType == "Up Out")
                { upout = true; }
                result = instrument.Barrier(upin, upout, downin, downout, sss.First(), k, v, r, t, boundary);
            }
            // digital option
            else if (example2.FirstOrDefault().TypeName == "Digital Option")
            {
                double rebate = example.First().Rebate;
                var ss = from a in data.InstrumentSetSet
                         where a.Id == insid
                         select a;
                string[] iDD = ss.First().Underlying.Split(' ');
                int realid = Convert.ToInt32(iDD[0]);
                var sss = from a in data.InstrumentSetSet
                          where a.Id == realid
                          select a.Strike;
                result = instrument.Digital(sss.First(), k, v, r, t, rebate);
            }
            // look back option
            else if (example2.FirstOrDefault().TypeName == "Look Back Option")
            {
                var ss = from a in data.InstrumentSetSet
                         where a.Id == insid
                         select a;
                string[] iDD = ss.First().Underlying.Split(' ');
                int realid = Convert.ToInt32(iDD[0]);
                var sss = from a in data.InstrumentSetSet
                          where a.Id == realid
                          select a.Strike;
                result = instrument.Lookback(sss.First(), k, v, t, r);
            }
            // range option
            else if (example2.FirstOrDefault().TypeName == "Range Option")
            {
                var ss = from a in data.InstrumentSetSet
                         where a.Id == insid
                         select a;
                string[] iDD = ss.First().Underlying.Split(' ');
                int realid = Convert.ToInt32(iDD[0]);
                var sss = from a in data.InstrumentSetSet
                          where a.Id == realid
                          select a.Strike;
                result = instrument.Range(sss.First(), k, r, v, t);
            }
            // Get values
            if (example.First().IsCall == "Call" || example.First().IsCall == "Stock")
            {
                // Buy Call
                if (tradebuy.Text == "Buy")
                {
                    marketprice = result[0] * Convert.ToDouble(tradequant.Text);
                    pl = (marketprice - Convert.ToInt32(tradeprice.Text)) * Convert.ToDouble(tradequant.Text);
                    delta = result[1] * Convert.ToDouble(tradequant.Text);
                    gamma = result[2] * Convert.ToDouble(tradequant.Text);
                    theta = result[3] * Convert.ToDouble(tradequant.Text);
                    rho = result[4] * Convert.ToDouble(tradequant.Text);
                    vega = result[5] * Convert.ToDouble(tradequant.Text);
                }
                // Sell Call
                else if (tradebuy.Text == "Sell")
                {
                    marketprice = result[0] * Convert.ToDouble(tradequant.Text);
                    pl = (Convert.ToInt32(tradeprice.Text) - marketprice) * Convert.ToDouble(tradequant.Text);
                    delta = -result[1] * Convert.ToDouble(tradequant.Text);
                    gamma = -result[2] * Convert.ToDouble(tradequant.Text);
                    theta = -result[3] * Convert.ToDouble(tradequant.Text);
                    rho = -result[4] * Convert.ToDouble(tradequant.Text);
                    vega = -result[5] * Convert.ToDouble(tradequant.Text);
                }
            }
            else if (example.First().IsCall == "Put")
            {
                if (tradebuy.Text == "Buy")
                {
                    marketprice = result[6] * Convert.ToDouble(tradequant.Text);
                    pl = (marketprice - Convert.ToInt32(tradeprice.Text)) * Convert.ToDouble(tradequant.Text);
                    delta = result[7] * Convert.ToDouble(tradequant.Text);
                    gamma = result[8] * Convert.ToDouble(tradequant.Text);
                    theta = result[9] * Convert.ToDouble(tradequant.Text);
                    rho = result[10] * Convert.ToDouble(tradequant.Text);
                    vega = result[11] * Convert.ToDouble(tradequant.Text);
                }
                else if (tradebuy.Text == "Sell")
                {
                    marketprice = result[6] * Convert.ToDouble(tradequant.Text);
                    pl = (Convert.ToInt32(tradeprice.Text) - marketprice) * Convert.ToDouble(tradequant.Text);
                    delta = -result[7] * Convert.ToDouble(tradequant.Text);
                    gamma = -result[8] * Convert.ToDouble(tradequant.Text);
                    theta = -result[9] * Convert.ToDouble(tradequant.Text);
                    rho = -result[10] * Convert.ToDouble(tradequant.Text);
                    vega = -result[11] * Convert.ToDouble(tradequant.Text);
                }
            }

            bool buy = false;
            if (tradebuy.Text == "Buy")
            { buy = true; }
            else { buy = false; }
            data.TradesSetSet.Add(new TradesSet
            {
                InstrumentSetId = example.First().Id,
                IsBuy = buy,
                Quantity = Convert.ToInt32(tradequant.Text),
                Price = Convert.ToInt32(tradeprice.Text),
                TimeStamp = DateTime.Now,
                Ticker = example.First().Ticker,
                MarketPrice = marketprice,
                PL = pl,
                Delta = delta,
                Gamma = gamma,
                Theta = theta,
                Rho = rho,
                Vega = vega
            });
            data.SaveChanges();
            tradebuy.Text = "";
            tradeid.Text = "";
            tradequant.Text = "";
            tradeprice.Text = "";

            data.PriceSetSet.Add(new PriceSet
            {
                InstrumentSetId = example.First().Id,
                Date = DateTime.Now.Date,
                ClosingPrice = marketprice
            });
            data.SaveChanges();
        }

        private void New_Trade_Load(object sender, EventArgs e)
        {
            Model1Container data = new Model1Container();
            var trade = from a in data.InstrumentSetSet
                        join b in data.InstTypeSetSet on a.InstTypeSetId equals b.Id
                        select new {a.InstTypeSetId,a.Id,a.CompanyName,typename = b.TypeName};
                        
            foreach (var i in trade)
            {
                string[] devided = i.typename.Split(' ');
                if (tradeid.Items.Contains(Convert.ToString(i.Id)) == false)
                {
                    tradeid.Items.Add(i.InstTypeSetId +" " +devided[0] +" " + i.Id + " " + i.CompanyName);
                }
            }
        }
    }
}
