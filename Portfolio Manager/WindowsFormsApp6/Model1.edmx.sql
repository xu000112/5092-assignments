
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/22/2018 10:43:00
-- Generated from EDMX file: C:\Users\Administrator\Desktop\Portfolio Manager\WindowsFormsApp6\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [5092DataBase];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_InstTypeSetInstrumentSet]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[InstrumentSetSet] DROP CONSTRAINT [FK_InstTypeSetInstrumentSet];
GO
IF OBJECT_ID(N'[dbo].[FK_InstrumentSetId]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PriceSetSet] DROP CONSTRAINT [FK_InstrumentSetId];
GO
IF OBJECT_ID(N'[dbo].[FK_InstrumentSetTradesSet]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TradesSetSet] DROP CONSTRAINT [FK_InstrumentSetTradesSet];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[InstTypeSetSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[InstTypeSetSet];
GO
IF OBJECT_ID(N'[dbo].[InstrumentSetSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[InstrumentSetSet];
GO
IF OBJECT_ID(N'[dbo].[PriceSetSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PriceSetSet];
GO
IF OBJECT_ID(N'[dbo].[TradesSetSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TradesSetSet];
GO
IF OBJECT_ID(N'[dbo].[InterestRateSetSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[InterestRateSetSet];
GO
IF OBJECT_ID(N'[dbo].[TotalSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TotalSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'InstTypeSetSet'
CREATE TABLE [dbo].[InstTypeSetSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TypeName] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'InstrumentSetSet'
CREATE TABLE [dbo].[InstrumentSetSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CompanyName] nvarchar(max)  NOT NULL,
    [Ticker] nvarchar(max)  NOT NULL,
    [Exchange] nvarchar(max)  NOT NULL,
    [Underlying] nvarchar(max)  NOT NULL,
    [Strike] float  NOT NULL,
    [Tenor] float  NOT NULL,
    [IsCall] nvarchar(max)  NOT NULL,
    [BarrierType] nvarchar(max)  NOT NULL,
    [Barriers] int  NOT NULL,
    [Rebate] int  NOT NULL,
    [InstTypeSetId] int  NOT NULL
);
GO

-- Creating table 'PriceSetSet'
CREATE TABLE [dbo].[PriceSetSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Date] datetime  NOT NULL,
    [ClosingPrice] float  NOT NULL,
    [InstrumentSetId] int  NOT NULL
);
GO

-- Creating table 'TradesSetSet'
CREATE TABLE [dbo].[TradesSetSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IsBuy] bit  NOT NULL,
    [Quantity] int  NOT NULL,
    [Price] float  NOT NULL,
    [TimeStamp] datetime  NOT NULL,
    [MarketPrice] float  NOT NULL,
    [PL] float  NOT NULL,
    [Delta] float  NOT NULL,
    [Gamma] float  NOT NULL,
    [Theta] float  NOT NULL,
    [Rho] float  NOT NULL,
    [Vega] float  NOT NULL,
    [Ticker] nvarchar(max)  NOT NULL,
    [InstrumentSetId] int  NOT NULL
);
GO

-- Creating table 'InterestRateSetSet'
CREATE TABLE [dbo].[InterestRateSetSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Tenor] int  NOT NULL,
    [Rate] float  NOT NULL
);
GO

-- Creating table 'TotalSet'
CREATE TABLE [dbo].[TotalSet] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [TotalPL] float  NOT NULL,
    [TotalDelta] float  NOT NULL,
    [TotalGamma] float  NOT NULL,
    [TotalTheta] float  NOT NULL,
    [TotalRho] float  NOT NULL,
    [TotalVega] float  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'InstTypeSetSet'
ALTER TABLE [dbo].[InstTypeSetSet]
ADD CONSTRAINT [PK_InstTypeSetSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'InstrumentSetSet'
ALTER TABLE [dbo].[InstrumentSetSet]
ADD CONSTRAINT [PK_InstrumentSetSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PriceSetSet'
ALTER TABLE [dbo].[PriceSetSet]
ADD CONSTRAINT [PK_PriceSetSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TradesSetSet'
ALTER TABLE [dbo].[TradesSetSet]
ADD CONSTRAINT [PK_TradesSetSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'InterestRateSetSet'
ALTER TABLE [dbo].[InterestRateSetSet]
ADD CONSTRAINT [PK_InterestRateSetSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [ID] in table 'TotalSet'
ALTER TABLE [dbo].[TotalSet]
ADD CONSTRAINT [PK_TotalSet]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [InstTypeSetId] in table 'InstrumentSetSet'
ALTER TABLE [dbo].[InstrumentSetSet]
ADD CONSTRAINT [FK_InstTypeSetInstrumentSet]
    FOREIGN KEY ([InstTypeSetId])
    REFERENCES [dbo].[InstTypeSetSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_InstTypeSetInstrumentSet'
CREATE INDEX [IX_FK_InstTypeSetInstrumentSet]
ON [dbo].[InstrumentSetSet]
    ([InstTypeSetId]);
GO

-- Creating foreign key on [InstrumentSetId] in table 'PriceSetSet'
ALTER TABLE [dbo].[PriceSetSet]
ADD CONSTRAINT [FK_InstrumentSetId]
    FOREIGN KEY ([InstrumentSetId])
    REFERENCES [dbo].[InstrumentSetSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_InstrumentSetId'
CREATE INDEX [IX_FK_InstrumentSetId]
ON [dbo].[PriceSetSet]
    ([InstrumentSetId]);
GO

-- Creating foreign key on [InstrumentSetId] in table 'TradesSetSet'
ALTER TABLE [dbo].[TradesSetSet]
ADD CONSTRAINT [FK_InstrumentSetTradesSet]
    FOREIGN KEY ([InstrumentSetId])
    REFERENCES [dbo].[InstrumentSetSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_InstrumentSetTradesSet'
CREATE INDEX [IX_FK_InstrumentSetTradesSet]
ON [dbo].[TradesSetSet]
    ([InstrumentSetId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------