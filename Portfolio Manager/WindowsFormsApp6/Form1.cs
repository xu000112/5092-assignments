﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp6
{
    public partial class Form1 : Form
    {
        public static double volotility;
        public Form1()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void instrumentTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            New_InstrumentType Form = new New_InstrumentType();
            Form.ShowDialog();
        }

        private void instrumentToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void tradeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            New_Trade Form = new New_Trade();
            Form.ShowDialog();
        }

        private void instrestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            New_Interest Form = new New_Interest();
            Form.ShowDialog();
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ActiveForm.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

            // TODO: 这行代码将数据加载到表“_5092DataBaseDataSet5.TradesSetSet”中。您可以根据需要移动或删除它。
            this.tradesSetSetTableAdapter.Fill(this._5092DataBaseDataSet5.TradesSetSet);
            volotility = Convert.ToDouble(vol.Text);
            Model1Container data = new Model1Container();
            var display = from a in data.TradesSetSet
                          select new
                          {
                              ID = a.Id,
                              IsBuy = a.IsBuy,
                              Quantity = a.Quantity,
                              Price = a.Price,
                              TimeStamp = a.TimeStamp,
                              MarketPrice = a.MarketPrice,
                              PL = a.PL,
                              Delta = a.Delta,
                              Gamma = a.Gamma,
                              Theta = a.Theta,
                              Rho = a.Rho,
                              Vega = a.Vega,
                              Ticker = a.Ticker,
                              InstrumentID = a.InstrumentSetId
                          };
            display.OrderBy(x => x.TimeStamp);
            trade.DataSource = display.ToList();
            var dis = from a in data.InstrumentSetSet
                      join b in data.InstTypeSetSet on a.InstTypeSetId equals b.Id
                      select new { a.Id, a.CompanyName, b.TypeName };
            foreach (var i in dis)
            {
                IDD.Items.Add(i.Id + " " + i.TypeName + ' ' + i.CompanyName);
            }
        }
        private void refresh_Click(object sender, EventArgs e)
        {
            volotility = Convert.ToDouble(vol.Text);
            Model1Container data = new Model1Container();
            IDD.Items.Clear();
            var dis = from a in data.InstrumentSetSet
                      join b in data.InstTypeSetSet on a.InstTypeSetId equals b.Id
                      select new { a.Id, a.CompanyName, b.TypeName };
            foreach (var i in dis)
            {
                IDD.Items.Add(i.Id + " " + i.TypeName + ' ' + i.CompanyName);
            }
            //
            if (IDD.Text == "")
            {
                var display10 = from a in data.PriceSetSet
                               join b in data.InstrumentSetSet on a.InstrumentSetId equals b.Id
                               join c in data.InstTypeSetSet on b.InstTypeSetId equals c.Id
                               select new
                               {
                                   ID = a.Id,
                                   a.ClosingPrice,
                                   a.Date,
                                   Company = b.CompanyName,
                                   Ticker = b.Ticker,
                                   Underlying = b.Underlying,
                                   Tenor = b.Tenor,
                                   IsCall = b.IsCall,
                                   type = c.TypeName
                               };
                data_analysis.DataSource = display10.ToList();
            }
            else if (IDD.Text != "")
            {
                string[] under = IDD.Text.Split(' ');
                double underlying = Convert.ToDouble(under[0]);
                var display10 = from a in data.PriceSetSet
                               join b in data.InstrumentSetSet on a.InstrumentSetId equals b.Id
                               join c in data.InstTypeSetSet on b.InstTypeSetId equals c.Id
                               where underlying == a.InstrumentSetId
                               select new
                               {
                                   ID = a.Id,
                                   a.ClosingPrice,
                                   a.Date,
                                   Company = b.CompanyName,
                                   Ticker = b.Ticker,
                                   Underlying = b.Underlying,
                                   Tenor = b.Tenor,
                                   IsCall = b.IsCall,
                                   type = c.TypeName
                               };
                data_analysis.DataSource = display10.ToList();
            }
            //
            var display = from a in data.TradesSetSet
                          select new
                          {
                              ID = a.Id,
                              IsBuy = a.IsBuy,
                              Quantity = a.Quantity,
                              Price = a.Price,
                              TimeStamp = a.TimeStamp,
                              MarketPrice = a.MarketPrice,
                              PL = a.PL,
                              Delta = a.Delta,
                              Gamma = a.Gamma,
                              Theta = a.Theta,
                              Rho = a.Rho,
                              Vega = a.Vega,
                              Ticker = a.Ticker,
                              InstrumentID = a.InstrumentSetId
                          };
            display.OrderBy(x => x.TimeStamp);
            trade.DataSource = display.ToList();
            //trade.Columns.Remove("InstrumentSet");
            //this.tradesSetSetTableAdapter.Fill(this._5092DataBaseDataSet5.TradesSetSet);
            var display2 = from a in data.InterestRateSetSet
                           select new
                           {
                               ID = a.Id,
                               Tenor = a.Tenor,
                               Rate = a.Rate
                           };
            var display3 = from a in data.InstTypeSetSet
                           select new
                           {
                               ID = a.Id,
                               Instrument_type = a.TypeName,
                           };
            var display4 = from a in data.PriceSetSet
                           join b in data.InstrumentSetSet on a.InstrumentSetId equals b.Id
                           join c in data.InstTypeSetSet on b.InstTypeSetId equals c.Id
                           select new
                           {
                               ID = a.Id,
                               Date = a.Date,
                               Closing_price = a.ClosingPrice,
                               InstrumentID = a.InstrumentSetId,
                               Company = b.CompanyName,
                               Instrument_Type = c.TypeName,
                               b.IsCall,
                               b.Ticker,
                               b.Underlying,
                               b.Exchange
                           };
            var display5 = from a in data.InstrumentSetSet
                           select new
                           {
                               ID = a.Id,
                               Company = a.CompanyName,
                               Ticker = a.Ticker,
                               Exchange = a.Exchange,
                               Underlying = a.Underlying,
                               Strike = a.Strike,
                               Tenor = a.Tenor,
                               IsCall = a.IsCall,
                               BarrierType = a.BarrierType,
                               Barrier = a.Barriers,
                               Rebate = a.Rebate,
                               InstrumentType = a.InstTypeSetId,
                           };
            var display6 = from a in data.TradesSetSet
                           select a;
            if (data_analysis.ColumnCount == 12)
            {
                data_analysis.DataSource = display5.ToList();
            }
            else if (data_analysis.ColumnCount == 10)
            {
                data_analysis.DataSource = display4.ToList();
            }
            else if (data_analysis.ColumnCount == 2)
            {
                data_analysis.DataSource = display3.ToList();
            }
            else if (data_analysis.ColumnCount == 3)
            {
                data_analysis.DataSource = display2.ToList();
            }
            else if (data_analysis.ColumnCount == 14)
            {
                data_analysis.DataSource = display6.ToList().OrderBy(x=>x.TimeStamp);
                data_analysis.Columns.Remove("InstrumentSet");
            }
        }

        private void priceBookToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }

        private void dataGridView3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //string data = dataGridView3.CurrentCell.Value.ToString();
        }

        private void dataGridView3_DragDrop(object sender, DragEventArgs e)
        {
        }

        private void dataGridView3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void historicalPriceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            New_Histprice from = new New_Histprice();
            from.ShowDialog();
        }

        private void priceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Model1Container data = new Model1Container();
            var display2 = from a in data.InstrumentSetSet
                           select new
                           {
                               ID = a.Id,
                               Company = a.CompanyName,
                               Ticker = a.Ticker,
                               Exchange = a.Exchange,
                               Underlying = a.Underlying,
                               Strike = a.Strike,
                               Tenor = a.Tenor,
                               IsCall = a.IsCall,
                               BarrierType = a.BarrierType,
                               Barrier = a.Barriers,
                               Rebate = a.Rebate,
                               InstrumentType = a.InstTypeSetId,
                           };
            data_analysis.DataSource = display2.ToList();
        }

        private void historyPriceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Model1Container data = new Model1Container();
            if (IDD.Text == "")
            {
                var display2 = from a in data.PriceSetSet
                               join b in data.InstrumentSetSet on a.InstrumentSetId equals b.Id
                               join c in data.InstTypeSetSet on b.InstTypeSetId equals c.Id
                               select new
                               {
                                   ID = a.Id,
                                   a.ClosingPrice,
                                   a.Date,
                                   Company = b.CompanyName,
                                   Ticker = b.Ticker,
                                   Underlying = b.Underlying,
                                   Tenor = b.Tenor,
                                   IsCall = b.IsCall,
                                   type = c.TypeName
                               };
                data_analysis.DataSource = display2.ToList();
            }
            else if (IDD.Text !="")
            {
                string[] under = IDD.Text.Split(' ');
                double underlying = Convert.ToDouble(under[0]);
                var display2 = from a in data.PriceSetSet
                               join b in data.InstrumentSetSet on a.InstrumentSetId equals b.Id
                               join c in data.InstTypeSetSet on b.InstTypeSetId equals c.Id
                               where underlying == a.InstrumentSetId
                               select new
                               {
                                   ID = a.Id,
                                   a.ClosingPrice,
                                   a.Date,
                                   Company = b.CompanyName,
                                   Ticker = b.Ticker,
                                   Underlying = b.Underlying,
                                   Tenor = b.Tenor,
                                   IsCall = b.IsCall,
                                   type = c.TypeName
                               };
                data_analysis.DataSource = display2.ToList();
            }
        }

        private void interestRateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Model1Container data = new Model1Container();
            var display2 = from a in data.InterestRateSetSet
                           select new
                           {
                               ID = a.Id,
                               Tenor = a.Tenor,
                               Rate = a.Rate
                           };
            data_analysis.DataSource = display2.ToList();
        }

        private void instrumentTypeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Model1Container data = new Model1Container();
            var display2 = from a in data.InstTypeSetSet
                           select new
                           {
                               ID = a.Id,
                               Instrument_type = a.TypeName,
                           };
            data_analysis.DataSource = display2.ToList();
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            Model1Container data = new Model1Container();
            // Get Instrumen
            List<TradesSet> datamodel = new List<TradesSet>();
            List<TradesSet> Others = new List<TradesSet>();
            for (int i = 0; i < trade.Rows.Count; i++)
            {
                if (trade.Rows[i].Selected == true)
                {
                    datamodel.Add(new TradesSet
                    {
                        Id = (int)trade.Rows[i].Cells[0].Value,
                        IsBuy = (bool)trade.Rows[i].Cells[1].Value,
                        Quantity = (int)trade.Rows[i].Cells[2].Value,
                        Price = (double)trade.Rows[i].Cells[3].Value,
                        TimeStamp = Convert.ToDateTime(trade.Rows[i].Cells[4].Value),
                        MarketPrice = (double)trade.Rows[i].Cells[5].Value,
                        PL = (double)trade.Rows[i].Cells[6].Value,
                        Delta = (double)trade.Rows[i].Cells[7].Value,
                        Gamma = (double)trade.Rows[i].Cells[8].Value,
                        Theta = (double)trade.Rows[i].Cells[9].Value,
                        Rho = (double)trade.Rows[i].Cells[10].Value,
                        Vega = (double)trade.Rows[i].Cells[11].Value,
                        Ticker = Convert.ToString(trade.Rows[i].Cells[12].Value),
                        InstrumentSetId = (int)trade.Rows[i].Cells[13].Value,
                    });
                }
                else
                {
                    Others.Add(new TradesSet
                    {
                        Id = (int)trade.Rows[i].Cells[0].Value,
                        IsBuy = (bool)trade.Rows[i].Cells[1].Value,
                        Quantity = (int)trade.Rows[i].Cells[2].Value,
                        Price = (double)trade.Rows[i].Cells[3].Value,
                        TimeStamp = Convert.ToDateTime(trade.Rows[i].Cells[4].Value),
                        MarketPrice = (double)trade.Rows[i].Cells[5].Value,
                        PL = (double)trade.Rows[i].Cells[6].Value,
                        Delta = (double)trade.Rows[i].Cells[7].Value,
                        Gamma = (double)trade.Rows[i].Cells[8].Value,
                        Theta = (double)trade.Rows[i].Cells[9].Value,
                        Rho = (double)trade.Rows[i].Cells[10].Value,
                        Vega = (double)trade.Rows[i].Cells[11].Value,
                        Ticker = Convert.ToString(trade.Rows[i].Cells[12].Value),
                        InstrumentSetId = (int)trade.Rows[i].Cells[13].Value,
                    });
                }
            }
            var model = from a in datamodel
                        join b in data.InstrumentSetSet on a.InstrumentSetId equals b.Id
                        join c in data.InstTypeSetSet on b.InstTypeSetId equals c.Id
                        select new { a, b, c };
            var interest = from a in data.InterestRateSetSet
                           select a;
            foreach (var i in model)
            {
                double r = 0;
                bool instruct = false;
                double ceil_tenor = 0;
                double floor_tenor = 0;
                double ceil_rate = 0;
                double floor_rate = 0;
                foreach (var j in interest.Distinct())
                {
                    if (j.Tenor - i.b.Tenor == 0)
                    {
                        r = j.Rate;
                        instruct = true;
                        break;
                    }
                    else
                    {
                        if (j.Tenor - i.b.Tenor < 0)
                        {
                            if (Math.Abs(j.Tenor - i.b.Tenor) < Math.Abs(ceil_tenor - i.b.Tenor))
                            {
                                ceil_tenor = j.Tenor;
                                ceil_rate = j.Rate;
                            }
                        }
                        if (j.Tenor - i.b.Tenor > 0)
                        {
                            if (Math.Abs(j.Tenor - i.b.Tenor) < Math.Abs(floor_tenor - i.b.Tenor))
                            {
                                floor_tenor = j.Tenor;
                                floor_rate = j.Rate;
                            }
                        }
                    }
                }
                if (instruct == false)
                {
                    r = (i.b.Tenor - ceil_tenor) / (floor_tenor - ceil_tenor) * (floor_rate - ceil_rate) + ceil_rate;
                }
                Montcalo.Main instrument = new Montcalo.Main();
                double v = Form1.volotility;
                double k = i.b.Strike;
                double t = i.b.Tenor;
                double[] result = new double[12];
                // stock
                if (i.c.TypeName == "Stock")
                {
                    var ss = from a in data.PriceSetSet
                             where a.InstrumentSetId == i.b.Id
                             orderby a.Date descending
                             select a.ClosingPrice;
                    result = instrument.Stock(ss.First(), r, v, t);
                }
                // European Option
                else if (i.c.TypeName == "European Option")
                {
                    string[] I = i.b.Underlying.Split(' ');
                    double IDD = Convert.ToDouble(I[0]);
                    var ss = from a in data.PriceSetSet
                             where a.InstrumentSetId == IDD
                             orderby a.Date descending
                             select a.ClosingPrice;
                    result = instrument.European(ss.First(), k, v, r, t);
                }
                // Asian option
                else if (i.c.TypeName == "Asian Option")
                {
                    string[] I = i.b.Underlying.Split(' ');
                    double IDD = Convert.ToDouble(I[0]);
                    var ss = from a in data.PriceSetSet
                             where a.InstrumentSetId == IDD
                             orderby a.Date descending
                             select a.ClosingPrice;
                    result = instrument.Asia(ss.First(), k, v, r, t);
                }
                // Barrier option !!!
                else if (i.c.TypeName == "Barrier Option")
                {
                    string[] I = i.b.Underlying.Split(' ');
                    double IDD = Convert.ToDouble(I[0]);
                    var ss = from a in data.PriceSetSet
                             where a.InstrumentSetId == IDD
                             orderby a.Date descending
                             select a.ClosingPrice;
                    double boundary = i.b.Barriers;
                    bool upin = false, upout = false, downin = false, downout = false;
                    if (i.b.BarrierType == "Down In")
                    { downin = true; }
                    else if (i.b.BarrierType == "Down Out")
                    { downout = true; }
                    else if (i.b.BarrierType == "Up In")
                    { upin = true; }
                    else if (i.b.BarrierType == "Up Out")
                    { upout = true; }
                    result = instrument.Barrier(upin, upout, downin, downout, ss.First(), k, v, r, t, boundary);
                }
                // digital option
                else if (i.c.TypeName == "Digital Option")
                {
                    string[] I = i.b.Underlying.Split(' ');
                    double IDD = Convert.ToDouble(I[0]);
                    var ss = from a in data.PriceSetSet
                             where a.InstrumentSetId == IDD
                             orderby a.Date descending
                             select a.ClosingPrice;
                    double rebate = i.b.Rebate;
                    result = instrument.Digital(ss.First(), k, v, r, t, rebate);
                }
                // look back option
                else if (i.c.TypeName == "Look Back Option")
                {
                    string[] I = i.b.Underlying.Split(' ');
                    double IDD = Convert.ToDouble(I[0]);
                    var ss = from a in data.PriceSetSet
                             where a.InstrumentSetId == IDD
                             orderby a.Date descending
                             select a.ClosingPrice;
                    result = instrument.Lookback(ss.First(), k, v, t, r);
                }
                // range option
                else if (i.c.TypeName == "Range Option")
                {
                    string[] I = i.b.Underlying.Split(' ');
                    double IDD = Convert.ToDouble(I[0]);
                    var ss = from a in data.PriceSetSet
                             where a.InstrumentSetId == IDD
                             orderby a.Date descending
                             select a.ClosingPrice;
                    result = instrument.Range(ss.First(), k, r, v, t);
                }
                if (i.b.IsCall == "Call" || i.b.IsCall == "Stock")
                {
                    // Buy Call
                    if (i.a.IsBuy == true)
                    {
                        i.a.MarketPrice = result[0] * i.a.Quantity;
                        i.a.PL = (i.a.MarketPrice - Convert.ToInt32(i.a.Price)) * i.a.Quantity;
                        i.a.Delta = result[1] * i.a.Quantity;
                        i.a.Gamma = result[2] * i.a.Quantity;
                        i.a.Theta = result[3] * i.a.Quantity;
                        i.a.Rho = result[4] * i.a.Quantity;
                        i.a.Vega = result[5] * i.a.Quantity;
                    }
                    // Sell Call
                    else if (i.a.IsBuy == false)
                    {
                        i.a.MarketPrice = result[0] * i.a.Quantity;
                        i.a.PL = (Convert.ToInt32(i.a.Price) - i.a.MarketPrice) * i.a.Quantity;
                        i.a.Delta = -result[1] * i.a.Quantity;
                        i.a.Gamma = -result[2] * i.a.Quantity;
                        i.a.Theta = -result[3] * i.a.Quantity;
                        i.a.Rho = -result[4] * i.a.Quantity;
                        i.a.Vega = -result[5] * i.a.Quantity;
                    }
                }
                else if (i.b.IsCall == "Put")
                {
                    if (i.a.IsBuy == true)
                    {
                        i.a.MarketPrice = result[6] * i.a.Quantity;
                        i.a.PL = (i.a.MarketPrice - Convert.ToInt32(i.a.Price)) * i.a.Quantity;
                        i.a.Delta = result[7] * i.a.Quantity;
                        i.a.Gamma = result[8] * i.a.Quantity;
                        i.a.Theta = result[9] * i.a.Quantity;
                        i.a.Rho = result[10] * i.a.Quantity;
                        i.a.Vega = result[11] * i.a.Quantity;
                    }
                    else if (i.a.IsBuy == false)
                    {
                        i.a.MarketPrice = result[6] * i.a.Quantity;
                        i.a.PL = (Convert.ToInt32(i.a.Price) - i.a.MarketPrice) * i.a.Quantity;
                        i.a.Delta = -result[7] * i.a.Quantity;
                        i.a.Gamma = -result[8] * i.a.Quantity;
                        i.a.Theta = -result[9] * i.a.Quantity;
                        i.a.Rho = -result[10] * i.a.Quantity;
                        i.a.Vega = -result[11] * i.a.Quantity;
                    }
                }
            }
            var model2 = from a in model
                         select a.a;
            List<TradesSet> model3 = model2.ToList();
            foreach (var i in Others)
            {
                model3.Add(i);
            }
            var model4 = from a in model3
                         orderby a.Id
                         select a;
            trade.DataSource = model4.ToList();
            
            //trade.Columns.Remove("InstrumentSet");
        
                //int m = 0;
                //foreach (var i in model)
                //{
                //    trade.Rows[m].Cells[6].Value = i.a.MarketPrice;
                //    trade.Rows[m].Cells[7].Value = i.a.PL;
                //    trade.Rows[m].Cells[8].Value = i.a.Delta;
                //    trade.Rows[m].Cells[9].Value = i.a.Gamma;
                //    trade.Rows[m].Cells[9].Value = i.a.Theta;
                //    trade.Rows[m].Cells[10].Value = i.a.Rho;
                //    trade.Rows[m].Cells[11].Value = i.a.Vega;
                //    m++;
                //}
        }

        private void trade_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            double marketPrice = 0;
            double pL = 0;
            double delta = 0;
            double gamma = 0;
            double theta = 0;
            double rho = 0;
            double vega = 0;
            for (int i = 0; i < trade.RowCount; i++)
            {
                if (trade.Rows[i].Cells[0].Selected == true)
                {
                    marketPrice = marketPrice + Convert.ToDouble(trade.Rows[i].Cells[5].Value);
                    pL = pL + Convert.ToDouble(trade.Rows[i].Cells[6].Value);
                    delta = delta + Convert.ToDouble(trade.Rows[i].Cells[7].Value);
                    gamma = gamma + Convert.ToDouble(trade.Rows[i].Cells[8].Value);
                    theta = theta + Convert.ToDouble(trade.Rows[i].Cells[9].Value);
                    rho = rho + Convert.ToDouble(trade.Rows[i].Cells[10].Value);
                    vega = vega + Convert.ToDouble(trade.Rows[i].Cells[11].Value);
                }
            }
            var dis = new List<sum_all>();
            dis.Add(new sum_all()
            {
                Marketprice = marketPrice,
                PL = pL,
                Delta = delta,
                Gamma = gamma,
                Theta = theta,
                Rho = rho,
                Vega = vega
            });
            //{ MarketPrice, PL, Delta = Delta, Gamma = Gamma, Theta = Theta, Rho = Rho, Vega = Vega }; 
            dataGridView2.DataSource = dis;
        }

        private void instrumentToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Edit_instrument data = new Edit_instrument();
            data.ShowDialog();
        }

        private void tradeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Edit_trade data = new Edit_trade();
            data.ShowDialog();
        }

        private void interestRateToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Edit_interest data = new Edit_interest();
            data.ShowDialog();
        }

        private void instrumentTypeToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Edit_InstrumentType data = new Edit_InstrumentType();
            data.ShowDialog();
        }

        private void historicalPriceToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Edit_Historicaldata data = new Edit_Historicaldata();
            data.ShowDialog();
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            New_Instrument Form = new New_Instrument();
            Form.ShowDialog();
        }

        private void stockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            New_instrument_stock data = new New_instrument_stock();
            data.ShowDialog();
        }

        private void tradeToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Model1Container data = new Model1Container();
            var display2 = from a in data.TradesSetSet
                           select a;
            data_analysis.DataSource = display2.ToList();
            data_analysis.Columns.Remove("InstrumentSet");
        }

        private void importAllDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Model1Container data = new Model1Container();
            var display2 = from a in data.PriceSetSet
                           join b in data.InstrumentSetSet on a.InstrumentSetId equals b.Id
                           join c in data.InstTypeSetSet on b.InstTypeSetId equals c.Id
                           select new
                           {
                               ID = a.Id,
                               Date = a.Date,
                               Closing_price = a.ClosingPrice,
                               InstrumentID = a.InstrumentSetId,
                               Company = b.CompanyName,
                               Instrument_Type = c.TypeName,
                               b.IsCall,
                               b.Ticker,
                               b.Underlying,
                               b.Exchange
                           };
            data_analysis.DataSource = display2.ToList();
        }

        private void importSelectedDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Model1Container data = new Model1Container();
            var display2 = from a in data.PriceSetSet
                           join b in data.InstrumentSetSet on a.InstrumentSetId equals b.Id
                           join c in data.InstTypeSetSet on b.InstTypeSetId equals c.Id
                           select new
                           {
                               ID = a.Id,
                               Date = a.Date,
                               Closing_price = a.ClosingPrice,
                               InstrumentID = a.InstrumentSetId,
                               Company = b.CompanyName,
                               Instrument_Type = c.TypeName,
                               b.IsCall,
                               b.Ticker,
                               b.Underlying,
                               b.Exchange
                           };
            data_analysis.DataSource = display2.ToList();
        }

        private void IDD_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
    public class sum_all
    {
        public double Marketprice { get; set; }
        public double PL { get; set; }
        public double Delta { get; set; }
        public double Gamma { get; set; }
        public double Theta { get; set; }
        public double Rho { get; set; }
        public double Vega { get; set; }
    }
}
