﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp6
{
    public partial class Edit_InstrumentType : Form
    {
        public Edit_InstrumentType()
        {
            InitializeComponent();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.instTypeSetSetTableAdapter.Update(this._5092DataBaseDataSet2.InstTypeSetSet);
        }

        private void Edit_InstrumentType_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“_5092DataBaseDataSet2.InstTypeSetSet”中。您可以根据需要移动或删除它。
            this.instTypeSetSetTableAdapter.Fill(this._5092DataBaseDataSet2.InstTypeSetSet);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ActiveForm.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
