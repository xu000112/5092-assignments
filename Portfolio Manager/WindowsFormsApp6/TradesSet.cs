//------------------------------------------------------------------------------
// <auto-generated>
//    此代码是根据模板生成的。
//
//    手动更改此文件可能会导致应用程序中发生异常行为。
//    如果重新生成代码，则将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace WindowsFormsApp6
{
    using System;
    using System.Collections.Generic;
    
    public partial class TradesSet
    {
        public int Id { get; set; }
        public bool IsBuy { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public System.DateTime TimeStamp { get; set; }
        public double MarketPrice { get; set; }
        public double PL { get; set; }
        public double Delta { get; set; }
        public double Gamma { get; set; }
        public double Theta { get; set; }
        public double Rho { get; set; }
        public double Vega { get; set; }
        public string Ticker { get; set; }
        public int InstrumentSetId { get; set; }
    
        public virtual InstrumentSet InstrumentSet { get; set; }
    }
}
