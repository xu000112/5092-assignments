﻿namespace WindowsFormsApp6
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.data_analysis = new System.Windows.Forms.DataGridView();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.instrumentTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.instrumentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tradeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.instrestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historicalPriceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dataAnalysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.priceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.interestRateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.instrumentTypeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tradeToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.editDataBaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.instrumentToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.interestRateToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.instrumentTypeToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.historicalPriceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tradeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.label4 = new System.Windows.Forms.Label();
            this.vol = new System.Windows.Forms.TextBox();
            this.refresh = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.trade = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this._5092DataBaseDataSet5 = new WindowsFormsApp6._5092DataBaseDataSet5();
            this.tradesSetSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tradesSetSetTableAdapter = new WindowsFormsApp6._5092DataBaseDataSet5TableAdapters.TradesSetSetTableAdapter();
            this.IDD = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.data_analysis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._5092DataBaseDataSet5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tradesSetSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // data_analysis
            // 
            this.data_analysis.AllowUserToAddRows = false;
            this.data_analysis.AllowUserToDeleteRows = false;
            this.data_analysis.AllowUserToOrderColumns = true;
            this.data_analysis.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.data_analysis.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.data_analysis.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.data_analysis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.data_analysis.GridColor = System.Drawing.SystemColors.Menu;
            this.data_analysis.Location = new System.Drawing.Point(3, 16);
            this.data_analysis.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.data_analysis.Name = "data_analysis";
            this.data_analysis.ReadOnly = true;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.data_analysis.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.data_analysis.RowTemplate.Height = 23;
            this.data_analysis.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.data_analysis.Size = new System.Drawing.Size(998, 244);
            this.data_analysis.TabIndex = 0;
            this.data_analysis.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.GridColor = System.Drawing.SystemColors.Menu;
            this.dataGridView2.Location = new System.Drawing.Point(3, 16);
            this.dataGridView2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowTemplate.Height = 23;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(998, 51);
            this.dataGridView2.TabIndex = 2;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.dataGridView2);
            this.groupBox5.Font = new System.Drawing.Font("Verdana", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(7, 65);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(1004, 70);
            this.groupBox5.TabIndex = 11;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Selected Total";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.data_analysis);
            this.groupBox6.Font = new System.Drawing.Font("Verdana", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(7, 402);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1004, 263);
            this.groupBox6.TabIndex = 12;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Data Analysis";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.dataAnalysisToolStripMenuItem,
            this.editDataBaseToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1016, 25);
            this.menuStrip1.TabIndex = 13;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.exitToolStripMenuItem,
            this.exitToolStripMenuItem1});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(39, 21);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.instrumentTypeToolStripMenuItem,
            this.instrumentToolStripMenuItem,
            this.tradeToolStripMenuItem,
            this.instrestToolStripMenuItem,
            this.historicalPriceToolStripMenuItem});
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.newToolStripMenuItem.Text = "New...";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // instrumentTypeToolStripMenuItem
            // 
            this.instrumentTypeToolStripMenuItem.Name = "instrumentTypeToolStripMenuItem";
            this.instrumentTypeToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.instrumentTypeToolStripMenuItem.Text = "Instrument Type";
            this.instrumentTypeToolStripMenuItem.Click += new System.EventHandler(this.instrumentTypeToolStripMenuItem_Click);
            // 
            // instrumentToolStripMenuItem
            // 
            this.instrumentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stockToolStripMenuItem,
            this.optionsToolStripMenuItem});
            this.instrumentToolStripMenuItem.Name = "instrumentToolStripMenuItem";
            this.instrumentToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.instrumentToolStripMenuItem.Text = "Instrument";
            this.instrumentToolStripMenuItem.Click += new System.EventHandler(this.instrumentToolStripMenuItem_Click);
            // 
            // stockToolStripMenuItem
            // 
            this.stockToolStripMenuItem.Name = "stockToolStripMenuItem";
            this.stockToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.stockToolStripMenuItem.Text = "Stock";
            this.stockToolStripMenuItem.Click += new System.EventHandler(this.stockToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.optionsToolStripMenuItem.Text = "Options";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // tradeToolStripMenuItem
            // 
            this.tradeToolStripMenuItem.Name = "tradeToolStripMenuItem";
            this.tradeToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.tradeToolStripMenuItem.Text = "Trade";
            this.tradeToolStripMenuItem.Click += new System.EventHandler(this.tradeToolStripMenuItem_Click);
            // 
            // instrestToolStripMenuItem
            // 
            this.instrestToolStripMenuItem.Name = "instrestToolStripMenuItem";
            this.instrestToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.instrestToolStripMenuItem.Text = "Interest Rate";
            this.instrestToolStripMenuItem.Click += new System.EventHandler(this.instrestToolStripMenuItem_Click);
            // 
            // historicalPriceToolStripMenuItem
            // 
            this.historicalPriceToolStripMenuItem.Name = "historicalPriceToolStripMenuItem";
            this.historicalPriceToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.historicalPriceToolStripMenuItem.Text = "Historical Price";
            this.historicalPriceToolStripMenuItem.Click += new System.EventHandler(this.historicalPriceToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(177, 6);
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.exitToolStripMenuItem1.Text = "Exit";
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.exitToolStripMenuItem1_Click);
            // 
            // dataAnalysisToolStripMenuItem
            // 
            this.dataAnalysisToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.priceToolStripMenuItem,
            this.interestRateToolStripMenuItem,
            this.instrumentTypeToolStripMenuItem1,
            this.tradeToolStripMenuItem2});
            this.dataAnalysisToolStripMenuItem.Name = "dataAnalysisToolStripMenuItem";
            this.dataAnalysisToolStripMenuItem.Size = new System.Drawing.Size(78, 21);
            this.dataAnalysisToolStripMenuItem.Text = "Data View";
            // 
            // priceToolStripMenuItem
            // 
            this.priceToolStripMenuItem.Name = "priceToolStripMenuItem";
            this.priceToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.priceToolStripMenuItem.Text = "Instrument Data";
            this.priceToolStripMenuItem.Click += new System.EventHandler(this.priceToolStripMenuItem_Click);
            // 
            // interestRateToolStripMenuItem
            // 
            this.interestRateToolStripMenuItem.Name = "interestRateToolStripMenuItem";
            this.interestRateToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.interestRateToolStripMenuItem.Text = "Interest Rate";
            this.interestRateToolStripMenuItem.Click += new System.EventHandler(this.interestRateToolStripMenuItem_Click);
            // 
            // instrumentTypeToolStripMenuItem1
            // 
            this.instrumentTypeToolStripMenuItem1.Name = "instrumentTypeToolStripMenuItem1";
            this.instrumentTypeToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.instrumentTypeToolStripMenuItem1.Text = "Instrument Type";
            this.instrumentTypeToolStripMenuItem1.Click += new System.EventHandler(this.instrumentTypeToolStripMenuItem1_Click);
            // 
            // tradeToolStripMenuItem2
            // 
            this.tradeToolStripMenuItem2.Name = "tradeToolStripMenuItem2";
            this.tradeToolStripMenuItem2.Size = new System.Drawing.Size(180, 22);
            this.tradeToolStripMenuItem2.Text = "Trade";
            this.tradeToolStripMenuItem2.Click += new System.EventHandler(this.tradeToolStripMenuItem2_Click);
            // 
            // editDataBaseToolStripMenuItem
            // 
            this.editDataBaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.instrumentToolStripMenuItem1,
            this.interestRateToolStripMenuItem1,
            this.instrumentTypeToolStripMenuItem2,
            this.historicalPriceToolStripMenuItem1,
            this.tradeToolStripMenuItem1});
            this.editDataBaseToolStripMenuItem.Name = "editDataBaseToolStripMenuItem";
            this.editDataBaseToolStripMenuItem.Size = new System.Drawing.Size(73, 21);
            this.editDataBaseToolStripMenuItem.Text = "Edit Data";
            // 
            // instrumentToolStripMenuItem1
            // 
            this.instrumentToolStripMenuItem1.Name = "instrumentToolStripMenuItem1";
            this.instrumentToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.instrumentToolStripMenuItem1.Text = "Instrument Data";
            this.instrumentToolStripMenuItem1.Click += new System.EventHandler(this.instrumentToolStripMenuItem1_Click);
            // 
            // interestRateToolStripMenuItem1
            // 
            this.interestRateToolStripMenuItem1.Name = "interestRateToolStripMenuItem1";
            this.interestRateToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.interestRateToolStripMenuItem1.Text = "Interest Rate";
            this.interestRateToolStripMenuItem1.Click += new System.EventHandler(this.interestRateToolStripMenuItem1_Click);
            // 
            // instrumentTypeToolStripMenuItem2
            // 
            this.instrumentTypeToolStripMenuItem2.Name = "instrumentTypeToolStripMenuItem2";
            this.instrumentTypeToolStripMenuItem2.Size = new System.Drawing.Size(180, 22);
            this.instrumentTypeToolStripMenuItem2.Text = "Instrument Type";
            this.instrumentTypeToolStripMenuItem2.Click += new System.EventHandler(this.instrumentTypeToolStripMenuItem2_Click);
            // 
            // historicalPriceToolStripMenuItem1
            // 
            this.historicalPriceToolStripMenuItem1.Name = "historicalPriceToolStripMenuItem1";
            this.historicalPriceToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.historicalPriceToolStripMenuItem1.Text = "Historical Price";
            this.historicalPriceToolStripMenuItem1.Click += new System.EventHandler(this.historicalPriceToolStripMenuItem1_Click);
            // 
            // tradeToolStripMenuItem1
            // 
            this.tradeToolStripMenuItem1.Name = "tradeToolStripMenuItem1";
            this.tradeToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.tradeToolStripMenuItem1.Text = "Trade";
            this.tradeToolStripMenuItem1.Click += new System.EventHandler(this.tradeToolStripMenuItem1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 41);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "Pricing Vol.(%):";
            // 
            // vol
            // 
            this.vol.Font = new System.Drawing.Font("Verdana", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vol.Location = new System.Drawing.Point(106, 38);
            this.vol.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.vol.Name = "vol";
            this.vol.Size = new System.Drawing.Size(89, 20);
            this.vol.TabIndex = 2;
            this.vol.Text = "0.5";
            this.vol.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // refresh
            // 
            this.refresh.BackColor = System.Drawing.SystemColors.HighlightText;
            this.refresh.Font = new System.Drawing.Font("Verdana", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refresh.Location = new System.Drawing.Point(225, 32);
            this.refresh.Name = "refresh";
            this.refresh.Size = new System.Drawing.Size(76, 31);
            this.refresh.TabIndex = 14;
            this.refresh.Text = "Refresh";
            this.refresh.UseVisualStyleBackColor = false;
            this.refresh.Click += new System.EventHandler(this.refresh_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.trade);
            this.groupBox4.Font = new System.Drawing.Font("Verdana", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(7, 141);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1004, 255);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Recent Trade(View Only)";
            // 
            // trade
            // 
            this.trade.AllowUserToAddRows = false;
            this.trade.AllowUserToDeleteRows = false;
            this.trade.AllowUserToOrderColumns = true;
            this.trade.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.trade.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.trade.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trade.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.trade.GridColor = System.Drawing.SystemColors.Menu;
            this.trade.Location = new System.Drawing.Point(3, 16);
            this.trade.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.trade.Name = "trade";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trade.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.trade.RowTemplate.Height = 23;
            this.trade.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.trade.Size = new System.Drawing.Size(998, 236);
            this.trade.TabIndex = 5;
            this.trade.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.trade_CellContentClick);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.HighlightText;
            this.button1.Font = new System.Drawing.Font("Verdana", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(324, 32);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(76, 31);
            this.button1.TabIndex = 16;
            this.button1.Text = "Price";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.HighlightText;
            this.button2.Font = new System.Drawing.Font("Verdana", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(425, 32);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(109, 31);
            this.button2.TabIndex = 17;
            this.button2.Text = "Portfolio Value";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // _5092DataBaseDataSet5
            // 
            this._5092DataBaseDataSet5.DataSetName = "_5092DataBaseDataSet5";
            this._5092DataBaseDataSet5.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tradesSetSetBindingSource
            // 
            this.tradesSetSetBindingSource.DataMember = "TradesSetSet";
            this.tradesSetSetBindingSource.DataSource = this._5092DataBaseDataSet5;
            // 
            // tradesSetSetTableAdapter
            // 
            this.tradesSetSetTableAdapter.ClearBeforeFill = true;
            // 
            // IDD
            // 
            this.IDD.Font = new System.Drawing.Font("Verdana", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDD.FormattingEnabled = true;
            this.IDD.Location = new System.Drawing.Point(573, 38);
            this.IDD.Name = "IDD";
            this.IDD.Size = new System.Drawing.Size(185, 20);
            this.IDD.TabIndex = 18;
            this.IDD.SelectedIndexChanged += new System.EventHandler(this.IDD_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1016, 669);
            this.Controls.Add(this.IDD);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.refresh);
            this.Controls.Add(this.vol);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "Form1";
            this.Text = "Wisdom";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.data_analysis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._5092DataBaseDataSet5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tradesSetSetBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView data_analysis;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem instrumentTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem instrumentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tradeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem instrestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem historicalPriceToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem dataAnalysisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem priceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem interestRateToolStripMenuItem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox vol;
        private System.Windows.Forms.Button refresh;
        private System.Windows.Forms.ToolStripMenuItem instrumentTypeToolStripMenuItem1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView trade;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ToolStripMenuItem editDataBaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem instrumentToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem interestRateToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem instrumentTypeToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem historicalPriceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tradeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem stockToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private _5092DataBaseDataSet5 _5092DataBaseDataSet5;
        private System.Windows.Forms.BindingSource tradesSetSetBindingSource;
        private _5092DataBaseDataSet5TableAdapters.TradesSetSetTableAdapter tradesSetSetTableAdapter;
        private System.Windows.Forms.ToolStripMenuItem tradeToolStripMenuItem2;
        private System.Windows.Forms.ComboBox IDD;
    }
}

