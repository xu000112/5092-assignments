﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp6
{
    public partial class Edit_trade : Form
    {
        public Edit_trade()
        {
            InitializeComponent();
        }

        private void Edit_trade_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“_5092DataBaseDataSet.TradesSetSet”中。您可以根据需要移动或删除它。
            this.tradesSetSetTableAdapter.Fill(this._5092DataBaseDataSet.TradesSetSet);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.tradesSetSetTableAdapter.Update(this._5092DataBaseDataSet.TradesSetSet);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ActiveForm.Close();
        }
    }
}
