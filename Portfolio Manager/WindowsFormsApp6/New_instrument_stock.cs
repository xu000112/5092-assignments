﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp6
{
    public partial class New_instrument_stock : Form
    {
        public New_instrument_stock()
        {
            InitializeComponent();
        }

        private void New_instrument_stock_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Model1Container data = new Model1Container();
            var stock = from a in data.InstTypeSetSet
                        where a.TypeName == "Stock"
                        select a.Id;
            data.InstrumentSetSet.Add(new InstrumentSet()
            {
                CompanyName = company.Text,
                Ticker = ticker.Text,
                Exchange = exchange.Text,
                Underlying = "Itself",
                Strike = Convert.ToDouble(strike.Text),
                Tenor = 1,
                IsCall = "Stock",
                BarrierType = "None",
                Barriers = 0,
                Rebate = 0,
                InstTypeSetId = stock.First(),
            });
            data.SaveChanges();
            var ID = from a in data.InstrumentSetSet
                     where a.InstTypeSetId == stock.FirstOrDefault()
                     select a.Id;
            data.PriceSetSet.Add(new PriceSet
            {
                InstrumentSetId = ID.Max(),
                Date = DateTime.Now.Date,
                ClosingPrice = Convert.ToDouble(strike.Text)
            });
            data.SaveChanges();
            company.Text = "";
            exchange.Text = "";
            strike.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ActiveForm.Close();
        }
    }
}
