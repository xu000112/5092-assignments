﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp6
{
    public partial class Edit_instrument : Form
    {
        public Edit_instrument()
        {
            InitializeComponent();
        }

        private void Edit_instrument_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“_5092DataBaseDataSet3.InstrumentSetSet”中。您可以根据需要移动或删除它。
            this.instrumentSetSetTableAdapter.Fill(this._5092DataBaseDataSet3.InstrumentSetSet);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.instrumentSetSetTableAdapter.Update(this._5092DataBaseDataSet3.InstrumentSetSet);
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ActiveForm.Close();
        }
    }
}
