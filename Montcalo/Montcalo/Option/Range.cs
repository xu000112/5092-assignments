﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Montcalo
{
    class Range : Main
    {
        private double[,] matrix;
        private int steplong;
        public double[] Generate(double s, double k, double r, double v, double t)
        {
            int number = trial;
            steplong = step;
            matrix = Rand.Polarmatrix(t,number);
            double Plus_s = getprice(s * 1.001, k, v, r, t, number);
            double Minus_s = getprice(s * 0.999, k, v, r, t, number);
            double Plus_t_call = getprice(s, k, v, r, t * 1.001, number);
            double Plus_r_call = getprice(s, k, v, r * 1.001, t, number);
            Call_price = getprice(s, k, v, r, t, number);
            Call_delta = (Plus_s - Call_price) / 0.001 / s;
            gamma = (Plus_s + Minus_s - 2 * Call_price) / 0.001 / s / 0.001 / s;
            vega = (getprice(s, k, v * 1.001, r, t, number) - Call_price) / 0.001 / v;
            Call_theta = -(Plus_t_call - Call_price) / 0.001 / t;
            Call_rho = (Plus_r_call - Call_price) / 0.001 / r;
            double[] result = new double[6];
            result[0] = Call_price;
            result[1] = Call_delta;
            result[2] = gamma;
            result[3] = Call_theta;
            result[4] = Call_rho;
            result[5] = vega;
            return result;
        }
        private void caculate(double t,double s0, double part1, double part2, double[] underlying_price, int j)
        {
            double si = 0, si_1 = s0;
            double max = si_1, min = si_1;
            for (int i = 0; i < steplong; i++)
            {
                si = si_1 * Math.Exp(part1 + part2 * matrix[j, i]);
                si_1 = si;
                max = Math.Max(si, max);
                min = Math.Min(si, min);
            }
            underlying_price[j] = max - min;
        }
        private double getprice(double s0, double k, double v, double r, double t, int number)
        {
            double[] underlying_price = new double[number];
            double dt = t / step;
            double part2 = v * Math.Sqrt(dt);
            double part1 = (r - (v * v) / 2) * dt;
            var o = new ParallelOptions();
            o.MaxDegreeOfParallelism = Thread;
            // Caculate underlying price and control variate 
            Parallel.For(0, number, o, j =>
            {
                caculate(t,s0, part1, part2, underlying_price, j);
            });
            // return option price
            return underlying_price.Sum() / number * Math.Exp(-r * t);
        }
    }
}
