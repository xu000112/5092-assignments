﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Montcalo
{
    class Barrier : Main
    {
        private double[,] matrix;
        private double steplong;
        public double[] Generate(bool upin, bool upout, bool downin, bool downout, double s, double k, double v, double r, double t, double boundary)
        {
            int number = trial;
            steplong = step;
            matrix = Rand.Polarmatrix(t, number);
            double Plus_s = getprice(upin, upout, downin, downout, boundary,"call", s * 1.001, k, v, r, t, number);
            double Minus_s = getprice(upin, upout, downin, downout, boundary, "call", s * 0.999, k, v, r, t, number);
            double Plus_s_put = getprice(upin, upout, downin, downout, boundary, "put", s * 1.001, k, v, r, t, number);
            double Minus_s_put = getprice(upin, upout, downin, downout, boundary, "put", s * 0.999, k, v, r, t, number);
            double Plus_t_call = getprice(upin, upout, downin, downout, boundary, "call", s, k, v, r, t * 1.001, number);
            double Plus_t_put = getprice(upin, upout, downin, downout, boundary, "put", s, k, v, r, t * 1.001, number);
            double Plus_r_call = getprice(upin, upout, downin, downout, boundary, "call", s, k, v, r * 1.001, t, number); ;
            double Plus_r_put = getprice(upin, upout, downin, downout, boundary, "put", s, k, v, r * 1.001, t, number);
            Call_price = getprice(upin, upout, downin, downout, boundary, "call", s, k, v, r, t, number);
            Put_price = getprice(upin, upout, downin, downout, boundary, "put", s, k, v, r, t, number);
            Call_delta = (Plus_s - Call_price) / 0.001 / s;
            Put_delta = (getprice(upin, upout, downin, downout, boundary, "put", s * 1.001, k, v, r, t, number) - Put_price) / 0.001 / s;
            gamma = (Plus_s + Minus_s - 2 * Call_price) / 0.001 / 0.001 / s / s;
            putgamma = (Plus_s_put + Minus_s_put - 2 * Put_price) / 0.001 / 0.001 / s / s;
            vega = (getprice(upin, upout, downin, downout, boundary, "call", s, k, v * 1.001, r, t, number) - Call_price) / 0.001 / v;
            putvega = (getprice(upin, upout, downin, downout, boundary, "put", s, k, v * 1.001, r, t, number) - Put_price) / 0.001 / v;
            Call_theta = -(Plus_t_call - Call_price) / 0.001 / t;
            Put_theta = -(Plus_t_put - Put_price) / 0.001 / t;
            Call_rho = (Plus_r_call - Call_price) / 0.001 / r;
            Put_rho = (Plus_r_put - Put_price) / 0.001 / r;
            double[] result = new double[12];
            result[0] = Call_price;
            result[1] = Call_delta;
            result[2] = gamma;
            result[3] = Call_theta;
            result[4] = Call_rho;
            result[5] = vega;
            result[6] = Put_price;
            result[7] = Put_delta;
            result[8] = putgamma;
            result[9] = Put_theta;
            result[10] = Put_rho;
            result[11] = putvega;
            return result;
        }
        public double getprice(bool upin, bool upout, bool downin, bool downout, double boundary, string what, double s0, double k, double v, double r, double t, int number)
        {
            double[] underlying_price = new double[number];
            double dt = t / step;
            double part2 = v * Math.Sqrt(dt);
            double part1 = (r - (v * v) / 2) * dt;
            // Caculate underlying price and control variate 
            if (upin == true)
            {
                for (int j = 0; j < number; j++)
                {
                    double si = 0, si_1 = s0, check = 0;
                    for (int i = 0; i < steplong; i++)
                    {
                        si = si_1 * Math.Exp(part1 + part2 * matrix[j, i]);
                        si_1 = si;
                        if (si > boundary)
                        { check = 1; }
                    }
                    if (check == 1)
                    { underlying_price[j] = si; }
                    else
                    { underlying_price[j] = k; }
                }
            }
            else if (downin == true)
            {
                for (int j = 0; j < number; j++)
                {
                    double si = 0, si_1 = s0, check = 0;
                    for (int i = 0; i < steplong; i++)
                    {
                        si = si_1 * Math.Exp(part1 + part2 * matrix[j, i]);
                        si_1 = si;
                        if (si < boundary)
                        { check = 1; }
                    }
                    if (check == 1)
                    { underlying_price[j] = si; }
                    else
                    { underlying_price[j] = k; }
                }
            }
            else if (upout == true)
            {
                for (int j = 0; j < number; j++)
                {
                    double si = 0, si_1 = s0, check = 0;
                    for (int i = 0; i < steplong; i++)
                    {
                        si = si_1 * Math.Exp(part1 + part2 * matrix[j, i]);
                        si_1 = si;
                        if (si > boundary)
                        { check = 1; }
                    }
                    if (check == 1)
                    { underlying_price[j] = k; }
                    else
                    { underlying_price[j] = si; }
                }
            }
            else if (downout == true)
            {
                for (int j = 0; j < number; j++)
                {
                    double si = 0, si_1 = s0, check = 0;
                    for (int i = 0; i < steplong; i++)
                    {
                        si = si_1 * Math.Exp(part1 + part2 * matrix[j, i]);
                        si_1 = si;
                        if (si < boundary)
                        { check = 1; }
                    }
                    if (check == 1)
                    { underlying_price[j] = k; }
                    else
                    { underlying_price[j] = si; }
                }
            }
            // return option price
            if (what == "call")
            {
                for (int i = 0; i < underlying_price.Count(); i++)
                    underlying_price[i] = Math.Max(underlying_price[i] - k, 0);
            }
            else
            {
                for (int i = 0; i < underlying_price.Count(); i++)
                    underlying_price[i] = Math.Max(k - underlying_price[i], 0);
            }
            return underlying_price.Sum() / underlying_price.Count() * Math.Exp(-r * t);
        }
    }
}
