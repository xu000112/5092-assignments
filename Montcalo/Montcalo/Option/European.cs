﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Montcalo
{
    class European : Main
    {
        private double[,] matrix;
        private int steplong;
        public double[] Generate(double s, double k, double v, double r, double t)
        {
            steplong = step;
            int number = trial;
            matrix = Rand.Polarmatrix(t,number);
            double Plus_s = getprice("no", "call", s + 3, k, v, r, t, number);
            double Minus_s = getprice("no", "call", s - 3, k, v, r, t, number);
            double Plus_t_call = getprice("no", "call", s, k, v, r, t + 0.00001, number);
            double Plus_t_put = getprice("no", "put", s, k, v, r, t + 0.00001, number);
            double Plus_r_call = getprice("no", "call", s, k, v, r + 0.01, t, number);
            double Plus_r_put = getprice("no", "put", s, k, v, r + 0.01, t, number);
            Call_price = getprice("Yes", "call", s, k, v, r, t, number);
            Put_price = getprice("Yes", "put", s, k, v, r, t, number);
            Call_delta = (Plus_s - Call_price) / 3;
            Put_delta = (getprice("no", "put", s + 3, k, v, r, t, number) - Put_price) / 3;
            gamma = (Plus_s + Minus_s - 2 * Call_price) / 9;
            putgamma = gamma;
            vega = (getprice("no", "call", s, k, v + 0.00001, r, t, number) - Call_price) / 0.00001;
            putvega = vega;
            Call_theta = -(Plus_t_call - Call_price) / 0.00001;
            Put_theta = -(Plus_t_put - Put_price) / 0.00001;
            Call_rho = (Plus_r_call - Call_price) / 0.01;
            Put_rho = (Plus_r_put - Put_price) / 0.01;

            double[] result = new double[12];
            result[0] = Call_price;
            result[1] = Call_delta;
            result[2] = gamma;
            result[3] = Call_theta;
            result[4] = Call_rho;
            result[5] = vega;
            result[6] = Put_price;
            result[7] = Put_delta;
            result[8] = putgamma;
            result[9] = Put_theta;
            result[10] = Put_rho;
            result[11] = putvega;
            return result;
        }
        public double getprice(string std, string what, double s0, double k, double v, double r, double t, int number)
        {
            List<double> control_variate = new List<double>();
            List<double> underlying_price = new List<double>();
            double dt = t / step;
            double part2 = v * Math.Sqrt(dt);
            double part1 = (r - (v * v) / 2) * dt;
            double erddt = Math.Exp(r * dt);
            double si = 0, si_1;
            // Caculate underlying price and control variate 
            for (int j = 0; j < number; j++)
            {
                si_1 = s0;
                for (int i = 0; i < steplong; i++)
                {
                    si = si_1 * Math.Exp(part1 + part2 * matrix[j, i]);
                    si_1 = si;
                }
                underlying_price.Add(si);
            }
            // return option price
            if (what == "call")
            {
                for (int i = 0; i < underlying_price.Count(); i++)
                {
                    if (underlying_price[i] - k > 0)
                        underlying_price[i] = (underlying_price[i] - k);
                    else
                        underlying_price[i] = 0;
                }
            }
            else if (what == "put")
            {
                for (int i = 0; i < underlying_price.Count; i++)
                {
                    if (underlying_price[i] - k < 0)
                        underlying_price[i] = (k - underlying_price[i]);
                    else
                        underlying_price[i] = 0;
                }
            }
           // Caculate new option price with lower deviation.
            return underlying_price.Sum() / trial * Math.Exp(-r * t);
           // return Standard Deviation
        }
    }
}