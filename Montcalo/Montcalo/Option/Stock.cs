﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Montcalo
{
    class Stock : Main
    {
        // Generate Everything which We want to caculate.
        public double[] Generate(double s, double r, double v, double t)
        {
            Call_delta = 1;
            Put_delta = -1;
            gamma = 0;
            putgamma = 0;
            Call_theta = 0;
            Put_theta = 0;
            Call_rho = 0;
            Put_rho = 0;
            vega = 0;
            putvega = 0;
            double[] result = new double[12];
            result[0] = s;
            result[1] = Call_delta;
            result[2] = gamma;
            result[3] = Call_theta;
            result[4] = Call_rho;
            result[5] = vega;
            result[7] = Put_delta;
            result[8] = putgamma;
            result[9] = Put_theta;
            result[10] = Put_rho;
            result[11] = putvega;
            return result;
        }
    }
}