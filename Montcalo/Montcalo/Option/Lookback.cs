﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Montcalo
{
    class Lookback : Main
    {
        private double[,] matrix;
        private int steplong;
        public double[] Generate(double s, double k, double v, double t, double r)
        {
            int number = trial;
            steplong = step;
            matrix = Rand.Polarmatrix(t,number);
            double Plus_s = getprice("call", s * 1.001, k, v, r, t, number);
            double Minus_s = getprice("call", s * 0.999, k, v, r, t, number);
            double Plus_s_put = getprice("put", s * 1.001, k, v, r, t, number);
            double Minus_s_put = getprice("put", s * 0.999, k, v, r, t, number);
            double Plus_t_call = getprice("call", s, k, v, r, t * 1.001, number);
            double Plus_t_put = getprice("put", s, k, v, r, t * 1.001, number);
            double Plus_r_call = getprice("call", s, k, v, r * 1.001, t, number);
            double Plus_r_put = getprice("put", s, k, v, r * 1.001, t, number);
            Call_price = getprice("call", s, k, v, r, t, number);
            Put_price = getprice("put", s, k, v, r, t, number);;
            Call_delta = (Plus_s - Call_price) / 0.001 / s;
            Put_delta = (getprice("put", s * 1.001, k, v, r, t, number) - Put_price) / 0.001 / s;
            gamma = (Plus_s + Minus_s - 2 * Call_price) / 0.001 / 0.001 / s / s;
            putgamma = (Plus_s_put + Minus_s_put - 2 * Put_price) / 0.001 / 0.001 / s / s;
            vega = (getprice("call", s, k, v * 1.001, r, t, number) - Call_price) / 0.001 / v;
            putvega = (getprice("put", s, k, v * 1.001, r, t, number) - Put_price) / 0.001 / v;
            Call_theta = -(Plus_t_call - Call_price) / 0.001 / t;
            Put_theta = -(Plus_t_put - Put_price) / 0.001 / t;
            Call_rho = (Plus_r_call - Call_price) / 0.001 / r;
            Put_rho = (Plus_r_put - Put_price) / 0.001 / r;
            double[] result = new double[12];
            result[0] = Call_price;
            result[1] = Call_delta;
            result[2] = gamma;
            result[3] = Call_theta;
            result[4] = Call_rho;
            result[5] = vega;
            result[6] = Put_price;
            result[7] = Put_delta;
            result[8] = putgamma;
            result[9] = Put_theta;
            result[10] = Put_rho;
            result[11] = putvega;
            return result;
        }
        private void caculate(double k, double s0, double t, double part1, double part2, string what, double[] underlying_price, int j)
        {
            double si = 0, si_1 = s0;
            double max = si_1, min = si_1;
            for (int i = 0; i < steplong; i++)
            {
                si = si_1 * Math.Exp(part1 + part2 * matrix[j, i]);
                si_1 = si;
                max = Math.Max(si, max);
                min = Math.Min(si, min);
            }
            if (what == "call")
            { underlying_price[j] = Math.Max(max - k, 0); }
            else
            { underlying_price[j] = Math.Max(k - min, 0); }
        }
        private double getprice(string what, double s0, double k, double v, double r, double t, int number)
        {
            double[] underlying_price = new double[number];
            double dt = t / step;
            double part2 = v * Math.Sqrt(dt);
            double part1 = (r - (v * v) / 2) * dt;
            // Caculate underlying price and control variate 
            var o = new ParallelOptions();
            o.MaxDegreeOfParallelism = Thread;
            Parallel.For(0, number, o, j =>
            {
                caculate(k, s0, t, part1, part2, what, underlying_price, j);
            });
            // return option price
            return underlying_price.Sum() / number * Math.Exp(-r * t);
        }
    }
}
