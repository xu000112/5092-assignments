﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Montcalo
{
    class Digital : Main
    {
        private double[,] matrix;
        private int steplong;
        public double[] Generate(double s, double k, double v, double r, double t, double rebate)
        {
            int number = trial;
            steplong = step;
            matrix = Rand.Polarmatrix(t,number);
            double Plus_s = getprice(rebate,"call", s * 1.001, k, v, r, t, number);
            double Minus_s = getprice(rebate, "call", s * 0.999, k, v, r, t, number);
            double Plus_s_put = getprice(rebate,"put", s * 1.001, k, v, r, t, number);
            double Minus_s_put = getprice(rebate, "put", s * 0.999, k, v, r, t, number);
            double Plus_t_call = getprice(rebate, "call", s, k, v, r, t * 1.001, number);
            double Plus_t_put = getprice(rebate,"put", s, k, v, r, t * 1.001, number);
            double Plus_r_call = getprice(rebate,"call", s, k, v, r * 1.001, t, number);
            double Plus_r_put = getprice(rebate,"put", s, k, v, r * 1.001, t, number);
            Call_price = getprice(rebate,"call", s, k, v, r, t, number);
            Put_price = getprice(rebate,"put", s, k, v, r, t, number);
            Call_delta = (Plus_s - Call_price) / 0.001 / s;
            Put_delta = (getprice(rebate,"put", s * 1.001, k, v, r, t, number) - Put_price) / 0.001 / s;
            gamma = (Plus_s + Minus_s - 2 * Call_price) / 0.001 / 0.001 / s / s;
            putgamma = (Plus_s_put + Minus_s_put - 2 * Put_price) / 0.001 / 0.001 / s / s;
            vega = (getprice(rebate,"call", s, k, v * 1.001, r, t, number) - Call_price) / 0.001 / v;
            putvega = (getprice(rebate,"put", s, k, v * 1.001, r, t, number) - Put_price) / 0.001 / v;
            Call_theta = -(Plus_t_call - Call_price) / 0.001 / t;
            Put_theta = -(Plus_t_put - Put_price) / 0.001 / t;
            Call_rho = (Plus_r_call - Call_price) / 0.001 / r;
            Put_rho = (Plus_r_put - Put_price) / 0.001 / r;
            double[] result = new double[12];
            result[0] = Call_price;
            result[1] = Call_delta;
            result[2] = gamma;
            result[3] = Call_theta;
            result[4] = Call_rho;
            result[5] = vega;
            result[6] = Put_price;
            result[7] = Put_delta;
            result[8] = putgamma;
            result[9] = Put_theta;
            result[10] = Put_rho;
            result[11] = putvega;
            return result;
        }
        private void caculate(double s0, double part1, double part2, double t, double[] underlying_price, int j)
        {
            double si = 0, si_1 = s0;
            for (int i = 0; i < steplong; i++)
            {
                si = si_1 * Math.Exp(part1 + part2 * matrix[j, i]);
                si_1 = si;
            }
            underlying_price[j] = si;
        }
        private double getprice(double rebate, string what, double s0, double k, double v, double r, double t, int number)
        {
            double[] underlying_price = new double[number];
            double dt = t / step;
            double part2 = v * Math.Sqrt(dt);
            double part1 = (r - (v * v) / 2) * dt;
            var o = new ParallelOptions();
            o.MaxDegreeOfParallelism = Thread;
            // Caculate underlying price and control variate 
            Parallel.For(0, number, o, j =>
            {
                caculate(s0, part1, part2, t, underlying_price, j);
            });
            // return option price
            if (what == "call")
            {
                for (int i = 0; i < underlying_price.Count(); i++)
                {
                    if (underlying_price[i] - k > 0)
                        underlying_price[i] = rebate;
                    else
                        underlying_price[i] = 0;
                }
            }
            else
            {
                for (int i = 0; i < underlying_price.Count(); i++)
                {
                    if (underlying_price[i] - k < 0)
                        underlying_price[i] = rebate;
                    else
                        underlying_price[i] = 0;
                }
            }
            return underlying_price.Sum() / trial * Math.Exp(-r * t);
        }
    }
}
