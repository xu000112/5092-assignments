﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Montcalo
{
    class Rand : Main
    {
        public static double[,] Polarmatrix(double t,int number)
        {
            double m, n, w, c;
            double[,] matrix = new double[number, step];
            int seed = Guid.NewGuid().GetHashCode();
            Random x = new Random(seed);
            for (int j = 0; j < number; j++)
            {
                for (int i = 0; i < step;)
                {
                    w = 2;
                    do
                    {
                        m = x.NextDouble() * 2 - 1.0;
                        n = x.NextDouble() * 2 - 1.0;
                        w = (n * n) + (m * m);
                    }
                    while (w > 1);
                    c = Math.Sqrt((-2 * Math.Log(w) / w));
                    if (step - i <= 1)
                    {
                        matrix[j, i] = c * m;
                        i++;
                    }
                    else
                    {
                        matrix[j, i] = c * n;
                        matrix[j, i + 1] = c * m;
                        i += 2;
                    }
                }
            }
            return matrix;
        }
    }
}
