﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Montcalo
{
    public class Main
    {
        protected static int trial = 10000, step = 50, Thread = 8;
        public double Stock_price, Call_price, Put_price, Call_delta, Put_delta, gamma, putgamma, vega, putvega, Call_theta, Put_theta, Call_rho, Put_rho;

        public double[] European(double s, double k, double v, double r, double t)
        {
            double[] result;
            European option = new European();
            result = option.Generate(s, k, v, r, t);
            return result;
        }
        public double[] Asia(double s, double k, double v, double r, double t)
        {
            double[] result;
            Asia option = new Asia();
            result = option.Generate(s, k, v,r, t);
            return result;
        }
        public double[] Barrier(bool upin, bool upout, bool downin, bool downout, double s, double k, double v, double r, double t, double boundary)
        {
            double[] result;
            Barrier option = new Barrier();
            result = option.Generate(upin,upout,downin,downout,s, k, v,r,t, boundary);
            return result;
        }
        public double[] Digital(double s, double k, double v, double r, double t, double rebate)
        {
            double[] result;
            Digital option = new Digital();
            result = option.Generate(s, k, v, r, t, rebate);
            return result;
        }
        public double[] Lookback(double s, double k, double v, double t, double r)
        {
            double[] result;
            Lookback option = new Lookback();
            result = option.Generate(s, k, v, t, r);
            return result;
        }
        public double[] Range(double s, double k, double r, double v, double t)
        {
            double[] result;
            Range option = new Range();
            result = option.Generate(s, k, r, v, t);
            return result;
        }
        public double[] Stock(double s, double r, double v, double t)
        {
            double[] result;
            Stock option = new Stock();
            result = option.Generate(s, r, v, t);
            return result;
        }
    }
}
